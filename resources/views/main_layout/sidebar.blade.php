 <?php $type = explode(",", Auth::user()->type); ?>    
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  
                  @if(in_array('administrator', $type) || in_array('admin_sekolah', $type))
                  <li><a href="/page/home"><i class="fa fa-home"></i> Home </a></li>
                  @endif

                  @if(in_array('administrator', $type) || in_array('wali_kelas', $type) || in_array('admin_sekolah', $type) || in_array('kepala_sekolah', $type))
                  <li><a href="/page/siswa"><i class="fa fa-edit"></i> Siswa </a></li>
                  <li><a><i class="fa fa-desktop"></i> Absensi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/page/absensi/hari-ini">Absensi Hari Ini</a></li>
                      <li><a href="/page/absensi/rekap-absensi/kelas">Rekap Absensi</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> GLS <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/page/gls/melihat">Melihat</a></li>
                      <li><a href="/page/gls/membaca">Membaca</a></li>
                      <li><a href="/page/gls/mendengar">Mendengar</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Karakter <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/page/karakter/opsi-pelanggaran">Opsi Pelanggaran</a></li>
                      <li><a href="/page/karakter/pelanggaran-siswa">Pelanggaran Siswa</a></li>                      
                      <li><a href="/page/karakter/tema-penilaian-karakter">Tema Penilaian Karakter</a></li>
                      <li><a href="/page/karakter/opsi-penilaian-karakter">Opsi Penilaian Karakter</a></li>
                    </ul>
                  </li>
                  @endif

                  @if(in_array('administrator', $type) || in_array('perpustakaan', $type))
                  <li><a><i class="fa fa-table"></i> Perpustakaan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/page/buku">Data Buku</a></li>
                      <li><a href="/page/buku/peminjaman">Peminjaman</a></li>

                    </ul>
                  </li>
                  @endif

                  @if(in_array('administrator', $type) || in_array('admin_sekolah', $type))
                  <li><a><i class="fa fa-table"></i> Sekolah <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/page/sekolah">Data Sekolah[S]</a></li>
                      <li><a href="/page/sekolah/admin">Data Admin Sekolah[S]</a></li>
                      <li><a href="/page/sekolah/anjungan">Data Anjungan[A]</a></li>
                      <li><a href="/page/sekolah/kelas">Data Kelas[A]</a></li>
                      <li><a href="/page/sekolah/tahun-ajaran">Data Tahun Ajaran[A]</a></li>
                      <li><a href="/page/sekolah/guru">Data Guru[A]</a></li>               
                    </ul>
                  </li>
                  @endif

                  @if(in_array('administrator', $type) || in_array('admin_sekolah', $type))
                    @if(in_array('administrator', $type))
                    <li><a href="/page/user"><i class="fa fa-user"></i> Manajemen User </a></li>
                    <li><a><i class="fa fa-star"></i> Manajemen Paket<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="/page/paket/">Manajemen Paket</a></li>
                        <li><a href="/page/paket/data">Data Paket</a></li>
                      </ul>
                    </li>  
                    <li><a><i class="fa fa-credit-card"></i> Pengaturan Kartu<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="/page/kartu/design">Layout Design</a></li>
                        <li><a href="/page/kartu/cetak">Cetak</a></li>
                      </ul>
                    </li>    
                    @endif              
                  <li><a href="/page/setting"><i class="fa fa-wrench"></i> Pengaturan </a></li>
                  @endif 
                </ul>
              </div>              

            </div>