@extends('main_layout.main')

@section('css')



@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Data buku</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#AddModal">Tambah Buku</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Data Buku<small>(kelas)</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <form action="/page/buku/save" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        {{ csrf_field() }}    
                        <input type="hidden" name="idbuku" value="{{ $data->id }}">         
                        <div class="form-group">
                          <label for="_judul">Input Barcode <span class="required">*</span></label>
                          <input type="text" class="form-control" id="_judul" name="perpus_barcode" required="required" value="{{ $data->perpustakaan_barcode }}" > <a href="">Generate</a>
                        </div>
                        <div class="form-group">
                          <label for="_konten">Nomor Isbn</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="perpus_isbn" value="{{ $data->perpustakaan_isbn }}">
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Judul Buku</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="perpus_judul" value="{{ $data->perpustakaan_judul_buku }}">
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Pengarang</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="perpus_pengarang" value="{{ $data->perpustakaan_pengarang }}">
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Penerbit</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="perpus_penerbit" value="{{ $data->perpustakaan_penerbit }}">
                        </div>
                        <div class="form-group">
                          <label for="_konten">Kode Buku</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="perpus_kode" value="{{ $data->perpustakaan_kode_buku }}">
                        </div>       
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
          
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')


@endsection

@section('modal')


 <!-- Modal -->
<div class="modal fade" id="AddModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Buku</h4>
      </div>
      <form action="/page/buku/save" method="post" enctype="multipart/form-data">
      <div class="modal-body">
          {{ csrf_field() }}          
          <div class="form-group">
            <label for="_judul">Input Barcode <span class="required">*</span></label>
            <input type="text" class="form-control" id="_judul" name="perpus_barcode" required="required"> <a href="">Generate</a>
          </div>
          <div class="form-group">
            <label for="_konten">Nomor Isbn</label>
            <input type="text" class="form-control" rows="5" id="_konten" name="perpus_isbn">
          </div> 
          <div class="form-group">
            <label for="_konten">Judul Buku</label>
            <input type="text" class="form-control" rows="5" id="_konten" name="perpus_judul">
          </div> 
          <div class="form-group">
            <label for="_konten">Pengarang</label>
            <input type="text" class="form-control" rows="5" id="_konten" name="perpus_pengarang">
          </div> 
          <div class="form-group">
            <label for="_konten">Penerbit</label>
            <input type="text" class="form-control" rows="5" id="_konten" name="perpus_penerbit">
          </div>
          <div class="form-group">
            <label for="_konten">Kode Buku</label>
            <input type="text" class="form-control" rows="5" id="_konten" name="perpus_kode">
          </div>       
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>

  </div>
</div>



@endsection

