@extends('main_layout.main')

@section('css')
@endsection

@section('content')
<div class="right_col" role="main">
    <div class="">
    <div class="clearfix"></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <h2>Peminjaman Buku | 
                <button type="button" class="btn btn-sm btn-success" data-toggle="modal" href='#modal-tp'><i class="glyphicon glyphicon-plus"></i> Tambah Peminjaman</button>
                <a class="btn btn-sm btn-primary" data-toggle="modal" href='#modal-peminjaman'>Dummy Peminjaman</a>
                </h2>
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Tanggal Pinjam</th>
                          <th>Tanggal Kembali</th>
                          <th>Kode Buku</th>
                          <th>Buku</th>
                          <th>Nama Siswa</th>
                          <th>NIS</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection

@section('modal')
<div class="modal fade" id="modal-tp">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Pinjaman</h4>

            </div>
            <div class="modal-body">
                    <div class="form-group">
                    <label>Scan Barcode/Qrcode buku :</label>
                    <input type="text" name="barcode" class="form-control" placeholder="Silahkan scan buku anda..">
                </div>

                <div class="form-group">
                    <label>Scan ID Card Siswa :</label>
                    <input type="text" name="barcode" class="form-control" placeholder="Silahkan scan ID card siswa..">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modal-peminjaman">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Peminjaman Buku</h4>
            </div>
            <div class="modal-body">
                
                <div class="row">
                    
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <h5>Peminjaman Aktif</h5>
                    </div>

                    
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <h5>Data Buku</h5>
                    </div>
                    
                    
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <h5>Detail Peminjaman</h5>
                    </div>
                    
                    
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

@endsection

 