@extends('main_layout.main')

@section('css')



@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tambah Data Buku</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>

                    <form action="/page/buku/save" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        {{ csrf_field() }}          
                        <div class="form-group">
                          <label for="_judul">Input Barcode <span class="required">*</span></label>
                          <input type="text" class="form-control" id="tb" name="perpus_barcode"> 
                          <input type="button" value="Generator!" onclick="randomValue();" />
                        </div>
                        <div class="form-group">
                          <label for="_konten">Nomor Isbn</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="perpus_isbn">
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Judul Buku</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="perpus_judul">
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Pengarang</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="perpus_pengarang">
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Penerbit</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="perpus_penerbit">
                        </div>
                        <div class="form-group">
                          <label for="_konten">Kode Buku</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="perpus_kode">
                        </div>       
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
          
                   


          
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')

<script type="text/javascript">

  function Random() {
    return Math.floor(Math.random() * 1000000000);
  }

  function randomValue() {
    document.getElementById('tb').value = Random();
  }

</script>


@endsection

@section('modal')



@endsection

