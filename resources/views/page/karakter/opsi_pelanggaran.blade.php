@extends('main_layout.main')

@section('css')



@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> Opsi Pelanggaran | <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#AddModal">Tambah Data Opsi Penilaian</button></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Pelanggaran</th>
                          <th>point</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $pelanggaran)

                        <tr>
                          <td>{{ $pelanggaran->nama_pelanggaran }}</td>
                          <td>{{ $pelanggaran->bobot_pelanggaran }}</td>
                          <td>
                            <a href="" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Ubah </a>
                            <a href="/page/karakter/penilaian/delete/{{ $pelanggaran->id }}" class="btn btn-danger btn-sm" onclick="return confirm('apakah anda ingin menghapus data ini?')"><i class="fa fa-trash-o"></i> Hapus </a>
                          </td>
                        </tr>
                        @endforeach 

                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')


@endsection

@section('modal')


 <!-- Modal -->
<div class="modal fade" id="AddModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Opsi Pelanggaran</h4>
      </div>
      <form action="/page/karakter/opsi-pelanggaran/add" method="post" enctype="multipart/form-data">
      <div class="modal-body">
          {{ csrf_field() }}  
          <div class="form-group">
            <label for="_konten">Pelanggaran</label>
            <input type="text" class="form-control" rows="5" id="_konten" name="pelanggaran_nama">
          </div> 
          <div class="form-group">
            <label for="_konten">Point</label>
            <input type="text" class="form-control" rows="5" id="_konten" name="pelanggaran_point">
          </div>     
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>

  </div>
</div>



@endsection

