@extends('main_layout.main')

@section('css')



@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Opsi Penilaian Karakter | <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#AddModal">Tambah Opsi Penilaian</button></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Tema Penilaian</th>
                          <th>Konten</th>
                          <th>Nilai</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $opsi_karakter)
                        <tr>
                          <td>{{ $opsi_karakter->tema_penilaian }}</td>
                          <td>{{ $opsi_karakter->opsi_konten }}</td>
                          <td>{{ $opsi_karakter->opsi_bobot_nilai }}</td>
                          <td>
                            <a href="/page/karakter/opsi-penilaian-karakter/delete/{{ $opsi_karakter->opid }}" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus </a>
                          </td>
                        </tr>
                        @endforeach                         
                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')


@endsection

@section('modal')


 <!-- Modal -->
<div class="modal fade" id="AddModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Opsi Penilaian Karakter</h4>
      </div>
      <form action="/page/karakter/opsi-penilaian-karakter/save" method="post" enctype="multipart/form-data">
      <div class="modal-body">
          {{ csrf_field() }}          
          <div class="form-group">
            <label for="_konten">Tema Penilaian</label>
            <select name="opsi_tema" class="form-control" required="required">
              <option value="">-Silahkan pilih-</option>
              @foreach(App\TemaPenilaianKarakter::all() as $d)
              <option value="{{$d->id}}">{{ $d->tema_penilaian }}</option>
              @endforeach
            </select>
          </div> 
          <div class="form-group">
            <label for="_konten">Konten</label> 
            <input type="text" class="form-control" name="opsi_konten" required="required">
          </div> 
          <div class="form-group">
            <label for="_konten">Bobot Nilai (Desimal menggunakan titik, cth: 3.5)</label>
            <input type="text" class="form-control" name="opsi_nilai" required="required">
          </div>         
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>

  </div>
</div>



@endsection

