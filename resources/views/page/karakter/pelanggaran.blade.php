@extends('main_layout.main')

@section('css')
<style type="text/css">
  .ui-autocomplete {
    z-index: 2150000000;
  }
</style>
@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Data Pelanggaran Siswa | <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#AddModal">Tambah Pelanggaran Siswa</button></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama Siswa</th>
                          <th>Pelanggaran</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $pelanggaran_siswa)
                        <tr>
                          <td>{{ $pelanggaran_siswa->siswa_nama}}</td>
                          <td>{{ $pelanggaran_siswa->nama_pelanggaran}}</td>
                          <td>
                            <a href="/page/karakter/pelanggaran-siswa/delete/{{ $pelanggaran_siswa->opid }}" class="btn btn-danger btn-sm"  onclick="return confirm('apakah anda ingin menghapus data ini?')"><i class="fa fa-trash-o"></i> Hapus </a>
                          </td>
                        </tr>
                        @endforeach                     

                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
  $( function() {
    $( "#tags" ).autocomplete({
      source: "/page/karakter/pelanggaran-siswa/get-siswa",
      minLength: 2
    });
  } );
  </script>
@endsection

@section('modal')


 <!-- Modal -->
<div class="modal fade" id="AddModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Pelanggaran Siswa</h4>
      </div>
      <form action="/page/karakter/pelanggaran-siswa/save" method="post" enctype="multipart/form-data">
      <div class="modal-body ui-front">
          {{ csrf_field() }}          
          <div class="form-group">
            <?php $data = \App\TahunAjaran::where('ta_status',1)->get()->first(); ?>
            Data pelanggaran ini akan disimpan pada data pelanggaran <code>Semester {{ $data->ta_semester }} Tahun Ajaran {{ $data->ta_tahun_ajaran }}.</code>
          </div>
          <div class="form-group">
            <label for="_konten">Nama Siswa</label>
            <input type="text" name="pilih_siswa" id="tags" class="form-control" required="required" >
          </div> 
          <div class="form-group">
            <label for="_konten">Pelanggaran</label>
            <select name="pilih_pelanggaran" class="form-control" required="required">
              <option value="">-Silahkan pilih-</option>
              @foreach(App\Opsi_Pelanggaran::all() as $d)
              <option value="{{$d->id}}">{{ $d->nama_pelanggaran }}</option>
              @endforeach
            </select>
          </div>       
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>

  </div>
</div>



@endsection

