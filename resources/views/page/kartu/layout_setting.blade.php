@extends('main_layout.main')

@section('css')
<style type="text/css">

	.CodeMirror {
		max-height: 100px !important;
	}

	.box {
      min-width: 57mm;
      max-width: 57mm;
      min-height: 88mm;
      max-height: 88mm;
      padding: 20px;
      text-align: center;
      margin: 20px;
      display: inline-block;
      position: relative;
    }

    .box p {
      padding: 0px;
      margin: 0px;
      text-align: center;
      font-size: 13px;
      line-height: 17px;
      margin-bottom: 5px;
    }

    .break{
      /*clear: both;*/
      page-break-after: always;
      /*margin-bottom: 30px;*/
    }

    .bottom-info {
      left: 5px;
      bottom: 5px;
      position: absolute;
      width: 100%;
    }

    .bottom-info p {
      text-align: left;
      font-family: sans-serif;
      margin: 0px;
      padding: 0px 5px 0px 0px;
    }

    .rules {
      margin: 0px;
      padding: 0px;
    }

    .rules li {
      text-align: justify;
      font-size: 10px;
      line-height: 10px;
      margin: 0px 0px 0px 5px;
      padding: 0px;
    }

    .ttd {
      margin-top: 20px;
    }

    .ttd p {
      text-align: center;
      font-size: 8px;
      padding: 0px;
      margin: 0px;
      line-height: 9px;
    }

</style>

<link rel="stylesheet" href="/vendors/codemirror/lib/codemirror.css">
<link rel="stylesheet" href="/vendors/codemirror/addon/hint/show-hint.css">
<script src="/vendors/codemirror/lib/codemirror.js"></script>
<script src="/vendors/codemirror/mode/css/css.js"></script>
<script src="/vendors/codemirror/addon/hint/show-hint.js"></script>
<script src="/vendors/codemirror/addon/hint/css-hint.js"></script>
@endsection

@section('content')
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Setting Layout Kartu</h3>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">

					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<form action="/page/kartu/design/front/save" method="POST" role="form">
		@csrf
		<input type="hidden" name="_id" value="{{ $data->first()->id }}">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Layout : {{ $data->first()->setting_title }}<small>Silahkan edit layout design kartu dibawah ini</small> <button type="button" class="btn btn-xs btn-primary" id="addElem">Tambahkan Elemen</button></h2>
					<button type="submit" class="btn btn-sm btn-danger pull-right">Simpan Perubahan</button>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="col-xs-5 col-sm-5 col-md-5 col-lg-2">
						<div class="box" style="background: url(/{{$setting->img}});background-position: center; background-size: cover; background-repeat: no-repeat;">  
							@if($element !== null)
								@foreach($element as $d)
									@if($d->type == "text")
									<p style="{{ $d->css }}" id="dcss_{{ $d->id }}">Ini Adalah Contoh Text</p>
									@elseif($d->type == "img")
									--
									@endif
								@endforeach
							@else
								<p style="" id="dcss_1">Ini Adalah Contoh Text</p>
							@endif
							<img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(150)->generate('abc')) }} ">
						</div>
					</div>
					<div class="col-xs-7 col-sm-7 col-md-7 col-lg-10 elem">
					@if($element !== null)
						@foreach($element as $d)
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<div class="setting-col">
								<div class="col-type">
									<label for="">Layout Element : {{ $d->id }}</label>

									<div class="form-group">
										<label for="">Tipe input :</label>
										<select name="type_{{ $d->id }}" id="input" class="form-control" required="required">
											<option value="-">- Silahkan pilih -</option>
											<option value="text">Input Text</option>
											<option value="nama">Nama Lengkap</option>
											<option value="ttd">Tanda Tangan</option>
											<option value="barcode">Barcode</option>
										</select>
									</div>

									<div class="form-group">
										<label for="">CSS :</label>
										<textarea class="form-control codemirror-textarea" id="css_{{ $d->id }}" required="required" >{{ str_replace(";", ";\n", $d->css) }}</textarea>
									</div>

									<!-- <input type="{{ $d->type }}" name="t{{ $d->id }}" id="t{{ $d->id }}" class="form-control" required="required" onkeypress="$('#t{{ $d->id }}').html($(this).val());console.log($(this).val());"> -->
								</div>
							</div>
						</div>
						@endforeach
					@else
					<script type="text/javascript">
						elemadd();
					</script>
					@endif
					</div>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>

@endsection

@section('js')

<script type="text/javascript">

	function recheck(id){
		CodeMirror.fromTextArea(document.getElementById("css_" + id), {
		    lineNumbers : true,
			mode: "text/css",
			matchBrackets: true,
			extraKeys: {"Ctrl-Space": "autocomplete"}
		}).on('change', function(cm){
			$('#dcss_'+id).attr("style", cm.getValue());
		});
	}

	function Generator() {};

	Generator.prototype.rand =  Math.floor(Math.random() * 26) + Date.now();

	Generator.prototype.getId = function() {
	   return this.rand++;
	};
	var idGen = new Generator();

	function elemadd(){
		var countelem = idGen.getId();

		$('.elem').append("<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\" id=\"elem_" + countelem + "\" style=\"border: 1px solid #ddd; padding: 10px;\"> <div class=\"setting-col\"> <div class=\"col-type\"> <label for=\"\"><button type=\"button\" class=\"btn btn-xs btn-danger\" onclick=\"$(this).parent().parent().parent().parent().remove();\"><i class=\"fa fa-close\"></i></button> Layout Element : " + countelem + "</label> <div class=\"form-group\"> <label for=\"\">Tipe input :</label> <select name=\"type[]\" id=\"input\" class=\"form-control\" required=\"required\"> <option value=\"-\">- Silahkan pilih -</option> <option value=\"text\">Input Text</option> <option value=\"nama\">Nama Lengkap</option> <option value=\"ttd\">Tanda Tangan</option> <option value=\"barcode\">Barcode</option> </select> </div> <div class=\"form-group\"> <label for=\"\">CSS :</label> <textarea class=\"form-control codemirror-textarea\" id=\"css_" + countelem + "\" name=\"set[]\"></textarea> </div> </div> </div> </div>");

		recheck(countelem);
	}

	$("#addElem").on('click', function(){
		elemadd();
	});
</script>
@endsection

@section('modal')
@endsection