@extends('main_layout.main')

@section('css')
@endsection

@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Cetak Kartu</h2>                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>  

                     <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama Sekolah</th>
                          <th>Cetak Guru</th>
                          <th>Cetak Siswa</th>
                          <th>Cetak Admin/Perpustakaan</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $d) 

                        <tr>
                          <td>{{ $d->nama_sekolah }}</td>
                          <td> <a href="/page/kartu/cetak/guru/all/<?php echo base64_encode($d->id); ?>" class="btn btn-primary btn-sm"><i class="fa fa-print"></i> cetak Semua Guru </a> </td>
                          <td> <a href="/page/kartu/cetak/siswa/all/<?php echo base64_encode($d->id); ?>" class="btn btn-danger btn-sm"><i class="fa fa-print"></i> cetak Semua Siswa </a></td> 
                          <td> <a href="/page/kartu/cetak/admin/all/<?php echo base64_encode($d->id); ?>" class="btn btn-success btn-sm"><i class="fa fa-print"></i> cetak Semua Admin </a></td> 
                        </tr>   

                        @endforeach                                                 
                      </tbody>
                    </table>    
                  </div>
                </div>
              </div>

          </div>
        </div>
@endsection

@section('js')
@endsection

@section('modal')
@endsection

