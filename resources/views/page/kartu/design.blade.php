@extends('main_layout.main')

@section('css')
@endsection

@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Upload Design</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" style="display: none;">          
                    <form action="/page/kartu/design/save" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}      
                        <div class="form-group">
                          <label for="_konten">Silahkan pilih</label>
                          <input type="file" class="form-control" rows="5" id="_konten" name="design_kartu">
                        </div>       
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>                    
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Design kartu</h2>                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>  

                     <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Design Kartu</th>
                          <th>Design Muka</th>
                          <th>Design Belakang</th>
                          <th width="30px"></th>
                        </tr>
                      </thead>
                      <tbody>
                       @foreach ($data as $d) 
                        <tr>
                          <td><img src="/thumb_design/{{ $d->design }}" class="img-responsive"></td>
                          <td>
                            <p><label>Design asset :</label></p>
                            <?php $dfront = \App\KartuSetting::where('id_design', $d->id)->where('setting_title', 'd_front')->get(); ?>
                            @if($dfront->isEmpty())
                              <p><a data-toggle="modal" href='#modal-upload-bg-depan' class="btn btn-default" onclick="$('#kid').val('<?php echo $d->id; ?>');">Tambah Background Design Muka</a></p>
                            @else
                              @if(isset($dfront->first()->setting_value))
                                <img width="200px" src="/{{ json_decode($dfront->first()->setting_value)->img }}">
                              @endif
                            @endif
                            <p><label>Design setting :</label></p>
                            <p>
                              @if($dfront->isEmpty())
                                <i>Tambahkan background design muka terlebih dahulu</i>
                              @else
                                @if(isset($dfront->first()->id))
                                  <a href="{{ url()->full() }}/{{ base64_encode($d->id) }}/layout/{{ base64_encode($dfront->first()->id) }}/front/style" class="btn btn-default">Edit Layout Design Muka</a>
                                @endif
                              @endif
                            </p>
                          </td>
                          <td>
                            <p><label>Design asset :</label></p>
                            @if($d->setting_title != 'd_back')
                            <p><a data-toggle="modal" href='#modal-upload-bg-belakang' class="btn btn-default" onclick="$('#kidb').val('<?php echo $d->kid; ?>');">Tambah Background Design Belakang</a></p>
                            @else
                              <img width="200px" src="/{{ json_decode($d->setting_value)->img }}">
                            @endif
                            <p><label>Design setting :</label></p>
                            <p>
                              @if($d->setting_title != 'd_back' && $d->setting_value == null)
                                Tambah layout design belakang terlebih dahulu
                              @else
                                <a href="{{ url()->full() }}/{{ base64_encode($d->kid) }}/layout/back/style" class="btn btn-default">
                                  Edit Layout Design Belakang
                                </a>
                              @endif
                            </p>
                          </td>
                          <td>
                            <button type="button" class="btn btn-danger"><i class="fa fa-close"></i></button>
                          </td>
                        </tr> 
                        @endforeach                         
                      </tbody>
                    </table>    
                  </div>
                </div>
              </div>

          </div>
        </div>
@endsection

@section('js')
@endsection

@section('modal')
<div class="modal fade" id="modal-upload-bg-depan">
  <div class="modal-dialog">
    <form action="{{ url()->full() }}/layout/front/bg" method="POST" role="form" enctype="multipart/form-data">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Upload Background Muka</h4>
      </div>
      <div class="modal-body">
          @csrf
          <input type="hidden" name="k_id" id="kid">
          <div class="form-group">
            <label for="">Tambahkan file</label>
            <input type="file" class="form-control" name="f_bg" required="required">
          </div>
        
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </div>
    </form>
  </div>
</div>

<div class="modal fade" id="modal-upload-bg-belakang">
  <div class="modal-dialog">
    <form action="{{ url()->full() }}/layout/back/bg" method="POST" role="form" enctype="multipart/form-data">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Upload Background Belakang</h4>
      </div>
      <div class="modal-body">
          @csrf
          <input type="hidden" name="k_id" id="kidb">
          <div class="form-group">
            <label for="">Tambahkan file</label>
            <input type="file" class="form-control" name="b_bg" required="required">
          </div>
        
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection

