@extends('main_layout.main')

@section('css')



@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Gerakan Literasi Sekolah </h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">

                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Membaca<small>Tabel Penilaian</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama</th>
                          <th>Buku</th>
                          <th>Halaman</th>
                          <th>Rangkuman</th>
                          <th>Waktu</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $membaca)

                        <tr>
                          <td>{{ $membaca->siswa_nama ?? "-"}}</td>
                          <td>{{ $membaca->perpustakaan_judul_buku ?? "-"}}</td>
                          <td>{{ $membaca->gls_halaman ?? "-"}}</td>
                          <td>{{ $membaca->gls_rangkuman ?? "-" }}</td>
                          <td>{{ date("d-M-Y H:i:s", strtotime($membaca->cat)) }}</td>
                          <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalLihat"><i class="fa fa-pencil"></i> Lihat </button>
                            <a href="/page/gls/membaca/delete/{{ $membaca->id }}" class="btn btn-danger btn-sm" onclick="return confirm('apakah anda ingin menghapus data ini?')"><i class="fa fa-trash-o"></i> Hapus </a>

                          </td>
                          </td>
                        </tr> 
                        @endforeach 

                      </tbody>
                    </table>          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')


@endsection

@section('modal')



@endsection

