@extends('main_layout.main')

@section('css')



@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Gerakan Literasi Sekolah </h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">

                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Melihat<small>Tabel Penilaian</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama</th>
                          <th>Rangkuman</th>
                          <th>Waktu</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $melihat)

                        <tr>
                          <td>{{ $melihat->siswa_nama }}</td>
                          <td>{{ $melihat->gls_rangkuman }}</td>
                          <td>{{ date("d-M-Y H:i:s", strtotime($melihat->cat)) }}</td>
                          <td>
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalLihat"><i class="fa fa-pencil"></i> Lihat </button>
                            <a href="/page/gls/melihat/delete/{{ $melihat->id }}" class="btn btn-danger btn-sm"  onclick="return confirm('apakah anda ingin menghapus data ini?')"><i class="fa fa-trash-o"></i> Hapus </a>
                          </td>
                        </tr> 
                        @endforeach 

                      </tbody>
                    </table>          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')


@endsection

@section('modal')


 <!-- Modal -->
<div class="modal fade" id="ModalLihat" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail GLS Melihat</h4>
      </div>
      <form action="/page/profil/save" method="post" enctype="multipart/form-data">
      <div class="modal-body">
          {{ csrf_field() }}          
          <div class="form-group">
            <label for="_judul">Nama Siswa </label>
            <span> : Nama </span>
          </div>
          <div class="form-group">
            <label for="_judul">Kelas </label>
            <span> : Kelas </span>
          </div>   
          <div class="form-group">
            <label for="_judul">Judul </label>
            <span> : Judul </span>
          </div>  
          <div class="form-group">
            <label for="_judul">Rangkuman </label>
            <span> : Rangkumanv </span>
          </div>  
          <div class="form-group">
            <label for="_judul">Tanggal </label>
            <span> : 12/12/2019 </span>
          </div>  
          <div class="form-group">
            <label for="_judul">Jam </label>
            <span> : 12.00 </span>
          </div>     
      </div>     
      </form>
    </div>

  </div>
</div>



@endsection

