@extends('main_layout.main')

@section('css')



@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Absensi Siswa {{ date('d-M-Y') }}</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">

                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Data Siswa<small>(kelas)</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <form action="/page/absensi/hari-ini" method="post">
                  <input type="hidden" name="kelas" value="{{$data->id}}">
                  {{ csrf_field() }}
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30"></p>          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nis</th>
                          <th>Nama Siswa</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($data->siswa as $index => $res)
                        <tr>
                          <td>{{ $res->siswa_nis}}</td>
                          <td>{{ $res->siswa_nama}}</td>
                          <td>
                            @if($data->absen->count()!=0)
                              @foreach($data->absen as $absen)
                                  @if($absen->siswa_nis==$res->siswa_nis && $absen->pivot->absensi_tanggal==Carbon\Carbon::now('Asia/Jakarta')->format('Y-m-d'))
                                    <p>{{$absen->pivot->absensi_status}}</p>
                                    @break
                                  @else
                                    @if($loop->last)
                                      <label class="radio-inline">
                                          <input id="aktif" type="radio" name="status[{{$index}}]" value="Hadir" checked>Hadir
                                      </label>
                                      <label class="radio-inline">
                                          <input id="aktif" type="radio" name="status[{{$index}}]" value="Alfa" >Alfa
                                      </label>
                                      <label class="radio-inline">
                                          <input id="aktif" type="radio" name="status[{{$index}}]" value="Izin" >Izin
                                      </label>
                                      <label class="radio-inline">
                                          <input id="aktif" type="radio" name="status[{{$index}}]" value="Sakit" >Sakit
                                      </label>
                                    @endif
                                  @endif
                              @endforeach
                              @else
                                <label class="radio-inline">
                                    <input id="aktif" type="radio" name="status[{{$index}}]" value="Hadir" checked>Hadir
                                </label>
                                <label class="radio-inline">
                                    <input id="aktif" type="radio" name="status[{{$index}}]" value="Alfa" >Alfa
                                </label>
                                <label class="radio-inline">
                                    <input id="aktif" type="radio" name="status[{{$index}}]" value="Izin" >Izin
                                </label>
                                <label class="radio-inline">
                                    <input id="aktif" type="radio" name="status[{{$index}}]" value="Sakit" >Sakit
                                </label>
                              @endif 
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>          
                  </div>
                  <div class="panel-footer">
                    <p data-placement="top" data-toggle="tooltip" title="Add" class="pull-right"><button class="btn btn-primary btn-sm" type="submit" data-title="Add">Submit</button></p>
                  </div>
                  </form>
                </div>
              </div>
          </div>
        </div>
@endsection

@section('js')


@endsection

@section('modal')

@endsection

