@extends('main_layout.main')

@section('css')



@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Rekap Absensi Kelas : .....</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">

                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Siswa<small>(kelas)</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <form action="/page/absensi/hari-ini" method="post">
                  <input type="hidden" name="kelas" value="{{$data->id}}">
                  {{ csrf_field() }}

                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">

                    </p>


          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nis</th>
                          <th>Nama Siswa</th>
                          <th>Hadir</th>
                          <th>Alfa</th>
                          <th>Ijin</th>
                          <th>Sakit</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($data->siswa as $index => $res)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $res->siswa_nis}}</td>
                            <td>{{ $res->siswa_nama}}</td>
                            <td>{{ count(App\Absensi::where(['id_siswa'=>$res->id, 'absensi_status'=>'hadir'])->get()) }}</td>
                            <td>{{ count(App\Absensi::where(['id_siswa'=>$res->id, 'absensi_status'=>'alfa'])->get()) }}</td>
                            <td>{{ count(App\Absensi::where(['id_siswa'=>$res->id, 'absensi_status'=>'izin'])->get()) }}</td>
                            <td>{{ count(App\Absensi::where(['id_siswa'=>$res->id, 'absensi_status'=>'sakit'])->get()) }}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>          
                  </div>
                  <div class="panel-footer">
                    <p data-placement="top" data-toggle="tooltip" title="Add" class="pull-right"><button class="btn btn-primary btn-sm" type="submit" data-title="Add">Submit</button></p>
                  </div>
                  </form>
                </div>
                
              </div>


          </div>
        </div>



@endsection

@section('js')


@endsection

@section('modal')

@endsection

