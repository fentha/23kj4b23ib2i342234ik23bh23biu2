@extends('main_layout.main')

@section('css')

<!-- bootstrap-datetimepicker -->
<link href="/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

  

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />





@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Data Absensi Bulan {{ date("M") }}
                    | <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#AddModal">Tambah Izin</a>
                </h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel input-daterange">
                  <div class="x_title">
                    <h2>Range rekap absensi 1</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content input-daterange">     
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
                      <label>Tanggal Mulai</label>     
                        <input type="text" name="from_date" id="from_date" class="form-control" >
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <label>Tanggal Akhir</label>     
                        <input type="text" name="to_date" id="to_date" class="form-control" >
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px;">
                     <button type="button" name="filter" id="filter" class="btn btn-info btn-sm">Filter</button>
                     <button type="button" name="refresh" id="refresh" class="btn btn-warning btn-sm">Refresh</button>
                    </div>
                    
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Absensi Siswa<small>(kelas)</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">

                    </p>
          
                    <table id="order_table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama Siswa</th>
                          <th>Absensi Kedatangan</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
          {{ csrf_field() }}
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')


  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

  <script src="/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>





<script>

$(document).ready(function(){

   var date = new Date();

   $('.input-daterange').datepicker({
    todayBtn: 'linked',
    format: 'yyyy-mm-dd',
    autoclose: true
  });

   var _token = $('input[name="_token"]').val();

   fetch_data();

   function fetch_data(from_date = '', to_date = '')
   {
    $.ajax({
     url:"{{ route('daterange.fetch_data') }}",
     method:"POST",
     data:{from_date:from_date, to_date:to_date, _token:_token},
     dataType:"json",
     success:function(data)
     {
      var output = '';
      $('#total_records').text(data.length);
      for(var count = 0; count < data.length; count++)
      {
       output += '<tr>';
       output += '<td>' + data[count].id_siswa + '</td>';
       output += '<td>' + data[count].absensi_waktu + '</td>';
       output += '<td>' + data[count].absensi_status + '</td></tr>';
     }
     $('tbody').html(output);
   }
 })
  }

  $('#filter').click(function(){
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    if(from_date != '' &&  to_date != '')
    {
     fetch_data(from_date, to_date);
   }
   else
   {
     alert('Pilih Tanggal Terlebih dahulu');
   }
 });

  $('#refresh').click(function(){
    $('#from_date').val('');
    $('#to_date').val('');
    fetch_data();
  });


});

</script>


@endsection

@section('modal')

@endsection

