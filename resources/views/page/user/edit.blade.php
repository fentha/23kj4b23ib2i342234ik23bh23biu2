@extends('main_layout.main')

@section('css')




@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Edit User</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                  
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit User<small>(kelas)</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <form action="/page/user/edit/save" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        {{ csrf_field() }}  
                        <input type="hidden" name="iduser" value="{{ $data->id }}">        
                        <div class="form-group">
                          <label for="_judul">Nama <span class="required">*</span></label>

                          <input type="text" class="form-control" id="_judul" name="name" required="required" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                          <label for="_konten">NIP
                            <button style="margin-top: -12px; margin-left: 10px;" type="button" class="btn btn-xs btn-warning" onclick="generate('guru');">
                            <i class="fa fa-spin load-generator-icon"></i>
                            <span class="load-generator-label">Generate NIP</span>
                          </button>
                        </label>
                          <input type="text" class="form-control" rows="5" id="_nip" name="nip" value="{{ $data->nip }}">
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Type</label>
                          <select class="form-control" name="type" required>
                            <option value="">-pilih salah satu-</option>
                            <option value="administrator" @if($data->type == "administrator") selected @endif>administrator</option>
                            <option value="admin_sekolah" @if($data->type == "admin_sekolah") selected @endif>admin sekolah</option>
                            <option value="wali_kelas" @if($data->type == "wali_kelas") selected @endif>wali kelas</option>
                            <option value="guru" @if($data->type == "guru") selected @endif>guru</option>
                            <option value="perpustakaan" @if($data->type == "perpustakaan") selected @endif>perpustakaan</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Sekolah</label>
                          <select class="form-control" name="sekolah" required>
                            <option value="">-pilih salah satu-</option>
                            @foreach(\App\Sekolah::all() as $d)
                            <option value="{{ $d->id }}" @if($data->id_sekolah == $d->id) selected @endif>{{ $d->nama_sekolah }}</option>
                            @endforeach
                          </select>
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Foto Profil</label>
                          <img src="/thumb_image/{{ $data->foto }}" class="img-responsive" style="margin-bottom: 10px;">                       
                          <input type="file" class="form-control" rows="5" id="_konten" name="s_foto">
                        </div>    
                        <div class="form-group">
                          <label for="_konten">Email</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="email" value="{{ $data->email }}">
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Password</label>
                          <input type="password" class="form-control" rows="5" id="_konten" name="password">
                        </div> 

                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
          
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')
<script type="text/javascript">
  function generate(type){
    $(".load-generator-icon").addClass("fa-spinner");
    $(".load-generator-label").html("Memproses pada server");

    $.ajax({
      type: "POST",
      url: '/ajax/idgenerator',
      data: JSON.stringify({
          _type: type,
          _token: "<?php echo csrf_token(); ?>"
      }),
      dataType: 'json',
      contentType: "application/json;charset=utf-8",
      success: function (res) {
        $(".load-generator-icon").removeClass("fa-spinner");
        $(".load-generator-label").html("Generate NIS");
        $("#_nip").val(res.id);
      }
    }).fail(function(xhr, status, error){
        console.log('error:' + status + ':' + error + ':' + xhr.responseText);
    });
  }
</script>
@endsection

@section('modal')



@endsection

