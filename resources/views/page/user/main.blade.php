@extends('main_layout.main')

@section('css')



@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Manajemen User</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#AddModal">Tambah User</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel User<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nama</th>
                          <th>NIP</th>
                          <th>Type</th>
                          <th>email</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $user) 
                        <tr>
                          <td>{{ $user->name }}</td>
                          <td>{{ $user->nip }}</td>
                          <td>{{ $user->type }}</td>
                          <td>{{ $user->email }}</td>
                          <td>
                            <a href="/page/user/edit/{{ $user->id }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Ubah </a>
                            <a href="/page/user/delete/{{ $user->id }}" class="btn btn-danger btn-sm" onclick="return confirm('apakah anda ingin menghapus user ini?')"><i class="fa fa-trash-o"></i> Hapus </a>
                          </td>
                        </tr>  
                        @endforeach  

                      </tbody>
                    </table>         
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')
<script type="text/javascript">
  function generate(type){
    $(".load-generator-icon").addClass("fa-spinner");
    $(".load-generator-label").html("Memproses pada server");

    $.ajax({
      type: "POST",
      url: '/ajax/idgenerator',
      data: JSON.stringify({
          _type: type,
          _token: "<?php echo csrf_token(); ?>"
      }),
      dataType: 'json',
      contentType: "application/json;charset=utf-8",
      success: function (res) {
        $(".load-generator-icon").removeClass("fa-spinner");
        $(".load-generator-label").html("Generate NIS");
        $("#_nip").val(res.id);
      }
    }).fail(function(xhr, status, error){
        console.log('error:' + status + ':' + error + ':' + xhr.responseText);
    });
  }
</script>
@endsection

@section('modal')


 <!-- Modal -->
<div class="modal fade" id="AddModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah User</h4>
      </div>
      <form action="/page/user/save" method="post" enctype="multipart/form-data">
      <div class="modal-body">
          {{ csrf_field() }}          
          <div class="form-group">
            <label for="_judul">Nama <span class="required">*</span></label>
            <input type="text" class="form-control" id="_judul" name="name" required="required" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Isikan judul !')">
          </div>
          <div class="form-group">
            <label for="_konten">NIP 
              <button style="margin-top: -12px; margin-left: 10px;" type="button" class="btn btn-xs btn-warning" onclick="generate('guru');">
                <i class="fa fa-spin load-generator-icon"></i>
                <span class="load-generator-label">Generate NIP</span>
              </button>
            </label>
            <input type="text" class="form-control" rows="5" id="_nip" name="nip">
          </div> 
          <div class="form-group">
            <label for="_konten">Type</label>
            <select class="form-control" rows="5" id="_konten" name="type">
              <option value="">-pilih salah satu-</option>
              <option value="administrator">administrator</option>
              <option value="admin_sekolah">admin sekolah</option>
              <option value="wali_kelas">wali kelas</option>
              <option value="guru">guru</option>
              <option value="perpustakaan">perpustakaan</option>
            </select>
          </div> 
          <div class="form-group">
            <label>Sekolah</label>
            <select class="form-control" name="sekolah" >
              <option value="">-pilih salah satu-</option>
              @foreach(\App\Sekolah::all() as $d)
              <option value="{{ $d->id }}">{{ $d->nama_sekolah }}</option>
              @endforeach
            </select>
          </div> 
          <div class="form-group">
            <label for="_konten">Foto Profil</label>
            <input type="file" class="form-control" rows="5" id="_konten" name="s_foto">
          </div>  
          <div class="form-group">
            <label for="_konten">Email</label>
            <input type="text" class="form-control" rows="5" id="_konten" name="email">
          </div> 
          <div class="form-group">
            <label for="_konten">Password</label>
            <input type="password" class="form-control" rows="5" id="_konten" name="password" >
          </div> 

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>

  </div>
</div>
@endsection


