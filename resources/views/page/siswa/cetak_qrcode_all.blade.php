<html> 
<head> 
  <title>Cetak  Barcode</title> 
  <style type="text/css" media="all">
    
    .box {
      min-width: 57mm;
      max-width: 57mm;
      min-height: 88mm;
      max-height: 88mm;
      padding: 20px;
      text-align: center;
      margin: 20px;
      display: inline-block;
      position: relative;
    }

    .box p {
      padding: 0px;
      margin: 0px;
      text-align: center;
      font-size: 13px;
      line-height: 17px;
      margin-bottom: 5px;
    }

    .break{
      /*clear: both;*/
      page-break-after: always;
      /*margin-bottom: 30px;*/
    }

    .bottom-info {
      left: 5px;
      bottom: 5px;
      position: absolute;
      width: 100%;
    }

    .bottom-info p {
      text-align: left;
      font-family: sans-serif;
      margin: 0px;
      padding: 0px 5px 0px 0px;
    }

    .rules {
      margin: 0px;
      padding: 0px;
    }

    .rules li {
      text-align: justify;
      font-size: 10px;
      line-height: 10px;
      margin: 0px 0px 0px 5px;
      padding: 0px;
    }

    .ttd {
      margin-top: 20px;
    }

    .ttd p {
      text-align: center;
      font-size: 8px;
      padding: 0px;
      margin: 0px;
      line-height: 9px;
    }


  </style>

</head> 
<!-- <body onload="window.print()">  -->
@if($type == 'siswa')
  <body> 
    <?php $cetak=1; ?>

    @foreach($data as $data) 
      @if($cetak==1)
      <div style="height: 100px;"></div>
      @endif

      <div class="box" style="background: url(/cc/or_bg_front.png);background-size: 68mm 99mm;">  
          <img style="margin: 0px auto 0px;" width="50px" src="/cc/gk.png">
          <div class="pp" style="background: url(/{{$data->siswa_foto}});width: 100px; height: 100px; background-size: contain; background-position: center; background-repeat: no-repeat; /*border-radius: 100px*/; margin: 15px auto 5px;"></div>
          <p style="font-variant-caps: all-petite-caps; font-size: 22px; font-weight: bold;margin-top: 10px;">{{$data->siswa_nama}}</p>
          <div class="bottom-info" style="">
            <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->margin(0)->size(120)->generate($data->siswa_nis)) }} " style="float: left; margin-right: 5px;">
            <p style="font-size: 16px; margin: 5px 0px 0px; font-family: monospace; font-weight: bold;">{{$data->siswa_nis}}</p>  
            <p style="font-size: 11px;">SD N PONJONG 1</p> 
            <p style="font-size: 10px; line-height: 12px;">Karangijo Wetan, Ponjong, Gunung Kidul 55892</p>
            <p style="font-size: 8px;">
              <img src="/cc/logo.png" style="width: 75px; filter: brightness(0); margin-top: 5px;">
            </p>     
          </div>
      </div>

      @if($cetak==4)
        <div class="break"></div>
        <?php $cetak=1; ?>
      @else
        <?php $cetak++; ?>
      @endif

    @endforeach 

    <!-- <div class="break"></div> -->
    <div class="box back-cover" style="background: url(/cc/or_bg_back.png);background-size: 68mm 99mm;">  
      <p style="font-size: 11px; text-align: center; font-weight: bold;">TATA TERTIB SISWA SD N PONJONG 1</p> 
        <ol start="1" class="rules">
          <li>Siswa harus hadir di sekolah paling lambat 5 menit sebelum pelajaran dimulai</li>
          <li>Siswa wajib mengikuti pelajaran dengan tenang,tertib & penuh kreatif sesuai dengan jadwal yang ditetapkan</li>
          <li>Setiap jam pertama dan terakhir diawali dengan berdoa dan penghormatan kepada guru. pada jam pertama ditambah dengan menyanyikan lagu indonesia raya</li>
          <li>Pada jam istirahat siswa harus keluar dari ruangan & tetap berada dilingkungan sekolah</li>
          <li>Siswa wajib minta ijin kepada guru piket bila akan meninggalkan sekolah</li>
          <li>Siswa wajib memelihara & menjaga ketertiban serta menjunjung nama baik sekolah</li>
          <li>Setiap siswa yang tidak dapat masuk sekolah, wajib memberikan surat dari orang tua kepada guru kelas</li>
          <li>Siswa wajib berpakaian sesuai dengan ketentuan yang ditetapkan sekolah</li>
          <li>Setiap siswa wajib menjaga & memelihara kebersihan sekolah</li>
          <li>Siswa wajib bersikap/berkarakter minimal “jujur dibeli saja” kepada siapapun</li>
        </ol>
      <div class="ttd">
        <p>Ponjong, Oktober 2019</p>
        <p>KEPALA SEKOLAH SD N PONJONG 1</p>
        <p><img height="30px" src="/cc/ttd.png"></p>
        <p style="font-weight: bold; text-decoration: underline;">AGUS JUMAIRI, M.Pd.</p>
        <p>NIP.196405011986041003</p>
      </div>
    </div>
  </body>
@else
  <body> 
    <?php $cetak=1; ?>

    @foreach($data as $data) 
      @if($cetak==1)
      <div style="height: 100px;"></div>
      @endif

      <div class="box" style="background: url(/cc/guru_bg_front.png);background-size: 68mm 99mm; border: 1px solid #ddd;">  
          <img style="margin: -13px 5px 0px 0px; width: 31px; float: left;" width="50px" src="/cc/gk.png">
          <p style="text-align: left; font-size: 14px; font-weight: bold; margin: -10px 0px -3px;">SD N PONJONG 1</p> 
          <p style="text-align: left; font-size: 9px; line-height: 10px; margin-top: 2px; ">Karangijo Wetan, Ponjong, Gunung Kidul 55892 Email : sdn1ponjong@gmail.com</p>
          <div class="pp" style="background: url(/{{$data->foto}});width: 100px; height: 100px; background-size: contain; background-position: center; background-repeat: no-repeat; /*border-radius: 100px*/; margin: 26px auto 5px;"></div>
          <p style="font-variant-caps: all-petite-caps; font-size: 22px; font-weight: bold;margin-top: 10px;">{{ $data->name }}</p>
          <div class="bottom-info" style="">
            @if($data->nip)
            <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->margin(0)->size(100)->generate($data->nip)) }} " style="float: left; margin: 5px;">
            @else
            <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->margin(0)->size(100)->generate(0)) }} " style="float: left; margin: 5px;">
            @endif
            <div style="right: 5px; font-size: 11px; padding-right: 5px; text-align: right; bottom: 5px; position: absolute;">
              <img src="/cc/logo.png" style="width: 75px; margin-top: 5px; padding-right: 5px; margin-bottom: -5px; filter: brightness(0);">
              <p style="font-size: 11px; margin: 5px 0px 0px; font-family: monospace; font-weight: bold; text-align: center;">{{$data->nip}}</p>  
            </div>     
          </div>
      </div>

      @if($cetak==4)
        <div class="break"></div>
        <?php $cetak=1; ?>
      @else
        <?php $cetak++; ?>
      @endif

    @endforeach 

    <!-- <div class="break"></div> -->
    <div class="box back-cover" style="background: url(/cc/guru_bg_back.png);background-size: 68mm 99mm;">  
        <p style="font-size: 12px; line-height: 13px; margin-top: 43px;">Kartu ini adalah milik SD Negeri Ponjong 1 dan wajib dipakai sebagaimana mestinya dalam menjalankan proses belajar mengajar. Kartu ini tidak dapat dipindahtangankan oleh siapapun.</p>
      <div class="ttd" style="margin-top: 135px;">
        <p>Ponjong, Oktober 2019</p>
        <p>KEPALA SEKOLAH SD N PONJONG 1</p>
        <p><img height="30px" src="/cc/ttd.png"></p>
        <p style="font-weight: bold; text-decoration: underline;">AGUS JUMAIRI, M.Pd.</p>
        <p>NIP.196405011986041003</p>
      </div>
    </div>
  </body>
@endif

</html>