@extends('main_layout.main')

@section('css')
@endsection

@section('content')
        <?php $type = explode(",", Auth::user()->type); ?>    
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    @if(!in_array('administrator', $type))
                      <h2>Tabel Data Siswa @if(isset($data_sekolah)): {{ $data_sekolah->nama_sekolah }} @endif </h2> @if(isset($data_sekolah)) <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#AddModal" style="margin-left: 10px;">Tambah Data Siswa</button> @endif
                    <!-- <a href="/page/siswa/print-qrcode-all" class="btn btn-success btn-sm"><i class="fa fa-print"></i> Cetak Qr-Code </a> -->
                    @else 
                      <h2>Tabel Data Siswa</h2>
                    @endif
                    <ul class="nav navbar-right panel_toolbox">
                      <li style="float: right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30"></p>
                    @if(!in_array('administrator', $type) || isset($_GET['show_admin']) )
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>NIS</th>
                          <th>Nama</th>
                          <th>Alamat</th>
                          <th>QrCode</th>
                          <!-- <th></th>-->
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $siswa) 
                        <tr>
                          <td>{{ $siswa->siswa_nis }}</td>
                          <td>{{ $siswa->siswa_nama }}</td>
                          <td>{{ $siswa->siswa_alamat }}</td>
                          <!-- 
                          <td>{!! QrCode::size(100)->generate($siswa->siswa_nis); !!}</td>
                          -->
                          <td style="width: 100px;">
                            <a href="/page/siswa/edit/{{ $siswa->id }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Ubah </a>
                            <a href="/page/siswa/delete/{{ $siswa->id }}" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus </a>
                            <a href="/page/siswa/gls/{{ $siswa->siswa_nis }}" class="btn btn-warning btn-sm"></i> Detail GLS </a>             

                            <!-- <a href="/page/siswa/print-qrcode/{{ $siswa->id }}" class="btn btn-success btn-sm"><i class="fa fa-print"></i> Cetak Qr-Code </a> -->
                          </td>
                        </tr> 
                        @endforeach  
                      </tbody>
                    </table>          
                    @else 
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sekolah</th>
                          <th>Jumlah Siswa</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach(\App\Siswa::selectRaw('sekolah.*, sekolah.id as id_sekolah, count(*) as jmlsiswa')->leftJoin('sekolah','sekolah.id','siswa.id_sekolah')->groupBy('sekolah.id')->get() as $d) 
                        <tr>
                          <td>{{ $d->nama_sekolah }}</td>
                          <td>{{ $d->jmlsiswa }} siswa</td>
                          <td style="width: 100px;">
                            <a href="{{ url()->current() }}/is_admin/{{ $d->id_sekolah }}?show_admin=true" class="btn btn-primary btn-sm"></i> Detail Siswa </a>             
                          </td>
                        </tr> 
                        @endforeach  
                      </tbody>
                    </table>  
                    @endif
                  </div>
                </div>
              </div>

          </div>
        </div>
@endsection

@section('js')
  <script type="text/javascript">
    function generate(type){
      $(".load-generator-icon").addClass("fa-spinner");
      $(".load-generator-label").html("Memproses pada server");

      $.ajax({
        type: "POST",
        url: '/ajax/idgenerator',
        data: JSON.stringify({
            _type: type,
            _token: "<?php echo csrf_token(); ?>"
        }),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        success: function (res) {
          $(".load-generator-icon").removeClass("fa-spinner");
          $(".load-generator-label").html("Generate NIS");
          $("#_nis").val(res.id);
        }
      }).fail(function(xhr, status, error){
          console.log('error:' + status + ':' + error + ':' + xhr.responseText);
      });
    }
  </script>
@endsection

@section('modal')


 <!-- Modal -->
<div class="modal fade" id="AddModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        @if(!in_array('administrator', $type))
          <h4 class="modal-title">Tambah Data Siswa @if(isset($data_sekolah)) pada Sekolah {{ $data_sekolah->nama_sekolah }} @else pada Sekolah {{ \App\Sekolah::find(Auth::user()->id_sekolah)->nama_sekolah }} @endif</h4>
        @else
           <h4 class="modal-title">Tambah Data Siswa</h4>
        @endif
      </div>
      <form action="/page/siswa/add" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          {{ csrf_field() }}      
          @if(isset($data_sekolah))
            <input type="hidden" name="id_sekolah" value="{{ $data_sekolah->id }}">  
          @else 
            <input type="hidden" name="id_sekolah" value="{{ Auth::user()->id_sekolah }}">  
          @endif
          <div class="form-group">
            <label for="_judul">NIS <span class="required">*</span>
              <button style="margin-top: -12px; margin-left: 10px;" type="button" class="btn btn-xs btn-warning" onclick="generate('siswa');">
                <i class="fa fa-spin load-generator-icon"></i>
                <span class="load-generator-label">Generate NIS</span>
              </button>
            </label>
            <input type="text" class="form-control" id="_nis" name="s_nis" required="required" >
          </div>
          <div class="form-group">
            <label for="_konten">Nama Siswa</label>
            <input type="text" class="form-control" name="s_nama" required="required">
          </div> 
          <div class="form-group">
            <label for="_konten">Kelas</label>
            
            <select name="s_kelas" class="form-control">
              <option value="">- Silahkan pilih -</option>
              @if(isset($data_sekolah))
                @foreach(\App\Kelas::where('id_sekolah', $data_sekolah->id)->get() as $das)
                  <option value="{{ $das->id }}">{{ $das->nama_kelas }}</option>
                @endforeach
              @else
                @foreach(\App\Kelas::where('id_sekolah', Auth::user()->id_sekolah)->get() as $das)
                  <option value="{{ $das->id }}">{{ $das->nama_kelas }}</option>
                @endforeach
              @endif
            </select>

          </div> 
          <div class="form-group">
            <label for="_konten">Alamat</label>
            <input type="text" class="form-control" name="s_alamat" required="required">
          </div>    
          <div class="form-group">
            <label for="_konten">Foto Profil</label>
            <input type="file" class="form-control" name="s_foto">
          </div>       
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>

  </div>
</div>



@endsection

