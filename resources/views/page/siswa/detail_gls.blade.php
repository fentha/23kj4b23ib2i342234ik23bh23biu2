@extends('main_layout.main')

@section('css')



@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Gerakan Literasi Sekolah </h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">

                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Membaca<small>Detail Data Literasi</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th width="20px;">No</th>
                          <th>Judul Buku</th>
                          <th>Halaman</th>
                          <th>Rangkuman</th>
                          <th>Waktu Dibuat</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        @foreach($data_membaca as $d)
                        <tr>
                          <td>{{ $no++ }}.</td>
                          <td>{{ $d->perpustakaan_judul_buku }}</td>
                          <td>{{ $d->gls_halaman }}</td>
                          <td>{{ $d->gls_rangkuman }}</td>
                          <td>{{ date('d-M-Y H:i:s', strtotime($d->cat)) }}</td>
                          <td></td>
                        </tr> 
                        @endforeach 

                      </tbody>
                    </table>          
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mendengar<small>Detail Data Literasi</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table class="table table-striped table-bordered dt-responsive nowrap datatable" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th width="20px;">No</th>
                          <th>Rangkuman</th>
                          <th>Waktu Dibuat</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        @foreach($data_mendengar as $d)
                        <tr>
                          <td>{{ $no++ }}.</td>
                          <td>{{ $d->gls_rangkuman }}</td>
                          <td>{{ date('d-M-Y H:i:s', strtotime($d->cat)) }}</td>
                          <td></td>
                        </tr> 
                        @endforeach 

                      </tbody>
                    </table>          
                  </div>
                </div>
              </div>

               <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Melihat<small>Detail Data Literasi</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table class="table table-striped table-bordered dt-responsive nowrap datatable" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th width="20px;">No</th>
                          <th>Rangkuman</th>
                          <th>Waktu Dibuat</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        @foreach($data_melihat as $d)
                        <tr>
                          <td>{{ $no++ }}.</td>
                          <td>{{ $d->gls_rangkuman }}</td>
                          <td>{{ date('d-M-Y H:i:s', strtotime($d->cat)) }}</td>
                          <td></td>
                        </tr> 
                        @endforeach 

                      </tbody>
                    </table>          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')
<script type="text/javascript">
  $('.datatable').dataTable();
</script>
@endsection

@section('modal')



@endsection

