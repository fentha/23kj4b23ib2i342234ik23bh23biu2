@extends('main_layout.main')

@section('css')
@endsection


@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3> Data Siswa</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">

          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Tabel Data Siswa<small>(kelas)</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <form action="/page/siswa/edit/save" method="post" enctype="multipart/form-data">
            <div class="modal-body">
              {{ csrf_field() }}  
              <input type="hidden" name="idsiswa" value="{{ $data->id }}">        
              <div class="form-group">
                <label for="_judul">NIS <span class="required">*</span>
                  <button style="margin-top: -12px; margin-left: 10px;" type="button" class="btn btn-xs btn-warning" onclick="generate('siswa');">
                    <i class="fa fa-spin load-generator-icon"></i>
                    <span class="load-generator-label">Generate NIS</span>
                  </button>
                </label>
                <input type="text" class="form-control" id="_s_nis" name="s_nis" required="required" value="{{ $data->siswa_nis }}" >
              </div>
              <div class="form-group">
                <label for="_konten">Nama Siswa</label>
                <input type="text" class="form-control" rows="5" id="_konten" name="s_nama" value="{{ $data->siswa_nama }}">
              </div> 
              <div class="form-group">
                <label for="_konten">Kelas</label>
                <input type="text" class="form-control" rows="5" id="_konten" name="s_kelas" value="">
              </div> 
              <div class="form-group">
                <label for="_konten">Alamat</label>
                <input type="text" class="form-control" rows="5" id="_konten" name="s_alamat" value="{{ $data->siswa_alamat }}">
              </div>    
              <div class="form-group">
                <label for="_konten">Foto Profil</label>
                <img src="/thumb_image/{{ $data->siswa_foto }}" class="img-responsive" style="margin-bottom: 10px;">                       
                <input type="file" class="form-control" rows="5" id="_konten" name="s_foto">
              </div>       
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
          
          
        </div>
      </div>
    </div>


  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  function generate(type){
    $(".load-generator-icon").addClass("fa-spinner");
    $(".load-generator-label").html("Memproses pada server");

    $.ajax({
      type: "POST",
      url: '/ajax/idgenerator',
      data: JSON.stringify({
          _type: type,
          _token: "<?php echo csrf_token(); ?>"
      }),
      dataType: 'json',
      contentType: "application/json;charset=utf-8",
      success: function (res) {
        $(".load-generator-icon").removeClass("fa-spinner");
        $(".load-generator-label").html("Generate NIS");
        $("#_s_nis").val(res.id);
      }
    }).fail(function(xhr, status, error){
        console.log('error:' + status + ':' + error + ':' + xhr.responseText);
    });
  }
</script>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="AddModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Profil Kejaksaan</h4>
      </div>
      <form action="/page/profil/save" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          {{ csrf_field() }}          
          <div class="form-group">
            <label for="_judul">NIS <span class="required">*</span></label>
            <input type="text" class="form-control" id="_judul" name="_judul" required="required" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Isikan judul !')">
          </div>
          <div class="form-group">
            <label for="_konten">Nama Siswa</label>
            <input type="text" class="form-control" rows="5" id="_konten" name="_konten">
          </div> 
          <div class="form-group">
            <label for="_konten">Kelas</label>
            <input type="text" class="form-control" rows="5" id="_konten" name="_konten">
          </div> 
          <div class="form-group">
            <label for="_konten">Alamat</label>
            <textarea type="text" class="form-control" rows="5" id="_konten" name="_konten"></textarea>
          </div>         
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

