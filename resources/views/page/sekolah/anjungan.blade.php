@extends('main_layout.main')

@section('css')
@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Data Anjungan Sekolah | <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#AddModal">Tambah Anjungan</button></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sekolah</th>
                          <th>Nama Anjungan</th>
                          <th>Login Email</th>
                          <th>Status</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $user) 
                        <tr>
                          <td>{{ $user->nama_sekolah ?? "-"}}</td>
                          <td>{{ $user->name }}</td>
                          <td>{{ $user->email }}</td>
                          <td>{{ $user->ust }}</td>
                          <td>
                            <a href="/page/sekolah/anjungan/edit/{{ $user->id }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Ubah </a>
                            <a href="/page/sekolah/anjungan/delete/{{ $user->id }}" class="btn btn-danger btn-sm" onclick="return confirm('apakah anda ingin menghapus sekolah ini?')"><i class="fa fa-trash-o"></i> Hapus </a>
                          </td>
                        </tr> 
                        @endforeach  
                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')


@endsection

@section('modal')


 <!-- Modal -->
<div class="modal fade" id="AddModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Anjungan Sekolah</h4>
      </div>
      <form action="/page/sekolah/anjungan/save" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          {{ csrf_field() }}        
          <div class="form-group">
            <label>Nama Anjungan <span class="required">*</span></label>
            <input type="text" class="form-control" name="user_nama" required="required" >
          </div>  
          <div class="form-group">
            <label>Sekolah <span class="required">*</span></label>
            <select name="user_sekolah" id="input" class="form-control" required="required">
              <option value="">- silahkan pilih -</option>
              @foreach(\App\Sekolah::orderBy('nama_sekolah','asc')->get() as $d)
              <option value="{{ $d->id }}">{{ $d->nama_sekolah }}</option>
              @endforeach
            </select>
          </div>   
          <div class="form-group">
            <label>Email <span class="required">*</span></label>
            <input type="email" class="form-control" name="user_email" required="required" >
          </div>
          <div class="form-group">
            <label>Password <span class="required">*</span></label>
            <input type="password" class="form-control" name="user_password" required="required" >
          </div>
          <div class="form-group">
            <label>Ulangi Password <span class="required">*</span></label>
            <input type="password" class="form-control" name="user_repassword" required="required" >
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>

  </div>
</div>



@endsection

