@extends('main_layout.main')

@section('css')




@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Data Tahun Ajaran | <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#AddModal">Tambah Tahun Ajaran</button></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>semester</th>
                          <th>Tahun Ajaran</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $tahunajaran) 
                        <tr>
                          <td>{{ $tahunajaran->ta_semester }}</td>
                          <td>{{ $tahunajaran->ta_tahun_ajaran }}</td>
                          <td>                            
                            <a href="/page/sekolah/tahun-ajaran/delete/{{ $tahunajaran->id }}" class="btn btn-danger btn-sm" onclick="return confirm('apakah anda ingin menghapus data tahun ajaran ini?')"><i class="fa fa-trash-o"></i> Hapus </a>
                          </td>
                        </tr> 
                        @endforeach  
                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')


@endsection

@section('modal')


 <!-- Modal -->
<div class="modal fade" id="AddModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Tahun Ajaran</h4>
      </div>
      <form action="/page/sekolah/tahun-ajaran/save" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          {{ csrf_field() }}        
          <div class="form-group">
            <label for="_judul">Semester <span class="required">*</span></label>
            <input type="text" class="form-control" id="_judul" name="tahun_semester" required="required" >
          </div>   
          <div class="form-group">
            <label for="_judul">Tahun Ajaran <span class="required">*</span></label>
            <input type="text" class="form-control" id="_judul" name="tahun_tahunajaran" required="required" >
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>

  </div>
</div>



@endsection

