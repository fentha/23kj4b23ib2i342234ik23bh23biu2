@extends('main_layout.main')

@section('css')




@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Data Siswa</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                  
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Data Siswa<small>(kelas)</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>
          
                    <form action="/page/siswa/edit/save" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        {{ csrf_field() }}  
                        <input type="hidden" name="idsiswa">        
                        <div class="form-group">
                          <label for="_judul">NIS <span class="required">*</span></label>
                          <input type="text" class="form-control" id="_judul" name="s_nis" required="required">
                        </div>
                        <div class="form-group">
                          <label for="_konten">Nama Siswa</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="s_nama">
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Kelas</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="s_kelas">
                        </div> 
                        <div class="form-group">
                          <label for="_konten">Alamat</label>
                          <input type="text" class="form-control" rows="5" id="_konten" name="s_alamat">
                        </div>    
                        <div class="form-group">
                          <label for="_konten">Foto Profil</label>
                          <img src="/thumb_image/" class="img-responsive" style="margin-bottom: 10px;">                       
                          <input type="file" class="form-control" rows="5" id="_konten" name="s_foto">
                        </div>       
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
          
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')


@endsection

@section('modal')


@endsection

