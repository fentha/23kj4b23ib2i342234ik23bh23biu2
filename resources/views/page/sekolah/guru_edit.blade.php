@extends('main_layout.main')

@section('css')




@endsection


@section('content')

        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Data Guru</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
     
                    </p>

                    <form action="/page/sekolah/guru/edit/save" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="idguru" value="{{ $data->id }}">         
                        <div class="form-group">
                          <label for="_judul">Nama Guru <span class="required">*</span></label>
                          <input type="text" class="form-control" id="_judul" name="user_nama" value="{{ $data->name}}" >
                        </div>   
                        <div class="form-group">
                          <label for="_judul">NIP <span class="required">*</span></label>
                          <input type="text" class="form-control" id="_judul" name="user_nip" value="{{ $data->nip}}"  >
                        </div>
                        <div class="form-group">
                          <label for="_judul">Alamat <span class="required">*</span></label>
                          <input type="text" class="form-control" id="_judul" name="user_alamat" value="{{ $data->alamat }}" >
                        </div>
                        <div class="form-group">
                          <label for="_judul">foto <span class="required">*</span></label>
                          <img src="/thumb_image/{{ $data->foto}}" class="img-responsive" style="width: 200px; margin-bottom: 10px;">
                          <input type="file" class="form-control" id="_judul" name="user_foto" >
                        </div>
                        <div class="form-group">
                          <label for="_judul">Email <span class="required">*</span></label>
                          <input type="email" class="form-control" id="_judul" name="user_email" value="{{ $data->email}}" >
                        </div>
                        <div class="form-group">
                          <label>Ubah Password <span class="required">*</span></label>
                          <input type="Password" class="form-control" name="user_password" >
                        </div>
                        <div class="form-group">
                          <label>Ulangi Password <span class="required">*</span></label>
                          <input type="password" class="form-control" name="user_repassword" >
                        </div>

                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
          
                   
          
          
                  </div>
                </div>
              </div>


          </div>
        </div>



@endsection

@section('js')


@endsection

@section('modal')


 <!-- Modal -->
<div class="modal fade" id="AddModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Sekolah</h4>
      </div>
      <form action="/page/sekolah/guru/save" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          {{ csrf_field() }}        
          <div class="form-group">
            <label for="_judul">Nama Guru <span class="required">*</span></label>
            <input type="text" class="form-control" id="_judul" name="user_nama" required="required" >
          </div>   
          <div class="form-group">
            <label for="_judul">NIP <span class="required">*</span></label>
            <input type="text" class="form-control" id="_judul" name="user_nip" required="required" >
          </div>
          <div class="form-group">
            <label for="_judul">Alamat <span class="required">*</span></label>
            <textarea type="text" class="form-control" id="_judul" name="user_alamat" required="required" ></textarea>
          </div>
          <div class="form-group">
            <label for="_judul">foto <span class="required">*</span></label>
            <input type="text" class="form-control" id="_judul" name="user_foto" required="required" >
          </div>
          <div class="form-group">
            <label for="_judul">Email <span class="required">*</span></label>
            <input type="email" class="form-control" id="_judul" name="user_email" required="required" >
          </div>
          <div class="form-group">
            <label for="_judul">Password <span class="required">*</span></label>
            <input type="Password" class="form-control" id="_judul" name="user_password" required="required" >
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>

  </div>
</div>



@endsection

