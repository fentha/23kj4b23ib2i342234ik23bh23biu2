--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: absensi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.absensi (
    id integer NOT NULL,
    id_siswa integer NOT NULL,
    absensi_waktu timestamp(0) without time zone NOT NULL,
    absensi_status character varying(255) DEFAULT 'masuk'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT absensi_absensi_status_check CHECK (((absensi_status)::text = ANY ((ARRAY['masuk'::character varying, 'terlambat'::character varying, 'pulang'::character varying, 'sudah_absensi'::character varying])::text[])))
);


ALTER TABLE public.absensi OWNER TO postgres;

--
-- Name: absensi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.absensi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.absensi_id_seq OWNER TO postgres;

--
-- Name: absensi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.absensi_id_seq OWNED BY public.absensi.id;


--
-- Name: gls; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gls (
    id integer NOT NULL,
    gls_kategori character varying(255) NOT NULL,
    id_buku integer,
    id_siswa integer NOT NULL,
    gls_halaman character varying(255),
    gls_rangkuman character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT gls_gls_kategori_check CHECK (((gls_kategori)::text = ANY ((ARRAY['membaca'::character varying, 'mendengar'::character varying, 'melihat'::character varying])::text[])))
);


ALTER TABLE public.gls OWNER TO postgres;

--
-- Name: gls_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gls_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gls_id_seq OWNER TO postgres;

--
-- Name: gls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gls_id_seq OWNED BY public.gls.id;


--
-- Name: ijin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ijin (
    id integer NOT NULL,
    id_siswa integer NOT NULL,
    ijin_tanggal_mulai timestamp(0) without time zone NOT NULL,
    ijin_tanggal_selesai timestamp(0) without time zone NOT NULL,
    ijin_status character varying(255) DEFAULT 'ijin'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT ijin_ijin_status_check CHECK (((ijin_status)::text = ANY ((ARRAY['ijin'::character varying, 'sakit'::character varying])::text[])))
);


ALTER TABLE public.ijin OWNER TO postgres;

--
-- Name: ijin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ijin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ijin_id_seq OWNER TO postgres;

--
-- Name: ijin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ijin_id_seq OWNED BY public.ijin.id;


--
-- Name: kelas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kelas (
    id integer NOT NULL,
    nama_kelas character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.kelas OWNER TO postgres;

--
-- Name: kelas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.kelas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kelas_id_seq OWNER TO postgres;

--
-- Name: kelas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.kelas_id_seq OWNED BY public.kelas.id;


--
-- Name: log_sistem; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.log_sistem (
    id integer NOT NULL,
    log_title character varying(255),
    log_value character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.log_sistem OWNER TO postgres;

--
-- Name: log_sistem_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.log_sistem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_sistem_id_seq OWNER TO postgres;

--
-- Name: log_sistem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.log_sistem_id_seq OWNED BY public.log_sistem.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_access_tokens (
    id character varying(100) NOT NULL,
    user_id bigint,
    client_id integer NOT NULL,
    name character varying(255),
    scopes text,
    revoked boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_access_tokens OWNER TO postgres;

--
-- Name: oauth_auth_codes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_auth_codes (
    id character varying(100) NOT NULL,
    user_id bigint NOT NULL,
    client_id integer NOT NULL,
    scopes text,
    revoked boolean NOT NULL,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_auth_codes OWNER TO postgres;

--
-- Name: oauth_clients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_clients (
    id integer NOT NULL,
    user_id bigint,
    name character varying(255) NOT NULL,
    secret character varying(100) NOT NULL,
    redirect text NOT NULL,
    personal_access_client boolean NOT NULL,
    password_client boolean NOT NULL,
    revoked boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_clients OWNER TO postgres;

--
-- Name: oauth_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oauth_clients_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth_clients_id_seq OWNER TO postgres;

--
-- Name: oauth_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oauth_clients_id_seq OWNED BY public.oauth_clients.id;


--
-- Name: oauth_personal_access_clients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_personal_access_clients (
    id integer NOT NULL,
    client_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_personal_access_clients OWNER TO postgres;

--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oauth_personal_access_clients_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth_personal_access_clients_id_seq OWNER TO postgres;

--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oauth_personal_access_clients_id_seq OWNED BY public.oauth_personal_access_clients.id;


--
-- Name: oauth_refresh_tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_refresh_tokens (
    id character varying(100) NOT NULL,
    access_token_id character varying(100) NOT NULL,
    revoked boolean NOT NULL,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_refresh_tokens OWNER TO postgres;

--
-- Name: opsi_pelanggaran; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.opsi_pelanggaran (
    id integer NOT NULL,
    id_admin integer NOT NULL,
    nama_pelanggaran character varying(255) NOT NULL,
    bobot_pelanggaran character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.opsi_pelanggaran OWNER TO postgres;

--
-- Name: opsi_pelanggaran_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.opsi_pelanggaran_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.opsi_pelanggaran_id_seq OWNER TO postgres;

--
-- Name: opsi_pelanggaran_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.opsi_pelanggaran_id_seq OWNED BY public.opsi_pelanggaran.id;


--
-- Name: opsi_penilaian_karakter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.opsi_penilaian_karakter (
    id integer NOT NULL,
    opsi_tema_penilaian character varying(255) NOT NULL,
    opsi_konten character varying(255) NOT NULL,
    opsi_bobot_nilai character varying(255) NOT NULL,
    id_admin integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.opsi_penilaian_karakter OWNER TO postgres;

--
-- Name: opsi_penilaian_karakter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.opsi_penilaian_karakter_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.opsi_penilaian_karakter_id_seq OWNER TO postgres;

--
-- Name: opsi_penilaian_karakter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.opsi_penilaian_karakter_id_seq OWNED BY public.opsi_penilaian_karakter.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: pelanggaran_siswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pelanggaran_siswa (
    id integer NOT NULL,
    id_siswa integer NOT NULL,
    id_opsi_pelanggaran integer NOT NULL,
    id_admin integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.pelanggaran_siswa OWNER TO postgres;

--
-- Name: pelanggaran_siswa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pelanggaran_siswa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pelanggaran_siswa_id_seq OWNER TO postgres;

--
-- Name: pelanggaran_siswa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pelanggaran_siswa_id_seq OWNED BY public.pelanggaran_siswa.id;


--
-- Name: penilaian_karakter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.penilaian_karakter (
    id integer NOT NULL,
    id_siswa integer NOT NULL,
    id_opsi_penilaian_karakter integer NOT NULL,
    id_admin integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.penilaian_karakter OWNER TO postgres;

--
-- Name: penilaian_karakter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.penilaian_karakter_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.penilaian_karakter_id_seq OWNER TO postgres;

--
-- Name: penilaian_karakter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.penilaian_karakter_id_seq OWNED BY public.penilaian_karakter.id;


--
-- Name: perpustakaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.perpustakaan (
    id integer NOT NULL,
    id_admin integer NOT NULL,
    perpustakaan_barcode character varying(255) NOT NULL,
    perpustakaan_isbn character varying(255) NOT NULL,
    perpustakaan_judul_buku character varying(255) NOT NULL,
    perpustakaan_pengarang character varying(255),
    perpustakaan_penerbit character varying(255),
    perpustakaan_kode_buku character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.perpustakaan OWNER TO postgres;

--
-- Name: perpustakaan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.perpustakaan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.perpustakaan_id_seq OWNER TO postgres;

--
-- Name: perpustakaan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.perpustakaan_id_seq OWNED BY public.perpustakaan.id;


--
-- Name: sekolah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sekolah (
    id integer NOT NULL,
    nama_sekolah character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.sekolah OWNER TO postgres;

--
-- Name: sekolah_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sekolah_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sekolah_id_seq OWNER TO postgres;

--
-- Name: sekolah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sekolah_id_seq OWNED BY public.sekolah.id;


--
-- Name: setting_admin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.setting_admin (
    id integer NOT NULL,
    id_admin integer NOT NULL,
    setting_title character varying(255) NOT NULL,
    setting_value text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.setting_admin OWNER TO postgres;

--
-- Name: setting_admin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.setting_admin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.setting_admin_id_seq OWNER TO postgres;

--
-- Name: setting_admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.setting_admin_id_seq OWNED BY public.setting_admin.id;


--
-- Name: siswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.siswa (
    id integer NOT NULL,
    id_sekolah integer,
    siswa_nis character varying(255) NOT NULL,
    siswa_nama character varying(255),
    siswa_alamat character varying(255),
    siswa_foto character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.siswa OWNER TO postgres;

--
-- Name: siswa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.siswa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.siswa_id_seq OWNER TO postgres;

--
-- Name: siswa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.siswa_id_seq OWNED BY public.siswa.id;


--
-- Name: siswa_kelas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.siswa_kelas (
    id integer NOT NULL,
    id_siswa integer NOT NULL,
    id_kelas integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.siswa_kelas OWNER TO postgres;

--
-- Name: siswa_kelas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.siswa_kelas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.siswa_kelas_id_seq OWNER TO postgres;

--
-- Name: siswa_kelas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.siswa_kelas_id_seq OWNED BY public.siswa_kelas.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: absensi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.absensi ALTER COLUMN id SET DEFAULT nextval('public.absensi_id_seq'::regclass);


--
-- Name: gls id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gls ALTER COLUMN id SET DEFAULT nextval('public.gls_id_seq'::regclass);


--
-- Name: ijin id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ijin ALTER COLUMN id SET DEFAULT nextval('public.ijin_id_seq'::regclass);


--
-- Name: kelas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kelas ALTER COLUMN id SET DEFAULT nextval('public.kelas_id_seq'::regclass);


--
-- Name: log_sistem id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.log_sistem ALTER COLUMN id SET DEFAULT nextval('public.log_sistem_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: oauth_clients id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_clients ALTER COLUMN id SET DEFAULT nextval('public.oauth_clients_id_seq'::regclass);


--
-- Name: oauth_personal_access_clients id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_personal_access_clients ALTER COLUMN id SET DEFAULT nextval('public.oauth_personal_access_clients_id_seq'::regclass);


--
-- Name: opsi_pelanggaran id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opsi_pelanggaran ALTER COLUMN id SET DEFAULT nextval('public.opsi_pelanggaran_id_seq'::regclass);


--
-- Name: opsi_penilaian_karakter id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opsi_penilaian_karakter ALTER COLUMN id SET DEFAULT nextval('public.opsi_penilaian_karakter_id_seq'::regclass);


--
-- Name: pelanggaran_siswa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pelanggaran_siswa ALTER COLUMN id SET DEFAULT nextval('public.pelanggaran_siswa_id_seq'::regclass);


--
-- Name: penilaian_karakter id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.penilaian_karakter ALTER COLUMN id SET DEFAULT nextval('public.penilaian_karakter_id_seq'::regclass);


--
-- Name: perpustakaan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perpustakaan ALTER COLUMN id SET DEFAULT nextval('public.perpustakaan_id_seq'::regclass);


--
-- Name: sekolah id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sekolah ALTER COLUMN id SET DEFAULT nextval('public.sekolah_id_seq'::regclass);


--
-- Name: setting_admin id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.setting_admin ALTER COLUMN id SET DEFAULT nextval('public.setting_admin_id_seq'::regclass);


--
-- Name: siswa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siswa ALTER COLUMN id SET DEFAULT nextval('public.siswa_id_seq'::regclass);


--
-- Name: siswa_kelas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siswa_kelas ALTER COLUMN id SET DEFAULT nextval('public.siswa_kelas_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: absensi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.absensi (id, id_siswa, absensi_waktu, absensi_status, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: gls; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gls (id, gls_kategori, id_buku, id_siswa, gls_halaman, gls_rangkuman, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: ijin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ijin (id, id_siswa, ijin_tanggal_mulai, ijin_tanggal_selesai, ijin_status, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: kelas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kelas (id, nama_kelas, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: log_sistem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.log_sistem (id, log_title, log_value, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000001_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2016_06_01_000001_create_oauth_auth_codes_table	1
4	2016_06_01_000002_create_oauth_access_tokens_table	1
5	2016_06_01_000003_create_oauth_refresh_tokens_table	1
6	2016_06_01_000004_create_oauth_clients_table	1
7	2016_06_01_000005_create_oauth_personal_access_clients_table	1
8	2019_08_19_121642_sekolah	1
9	2019_08_19_121643_siswa	1
10	2019_08_19_121644_kelas	1
11	2019_08_19_122307_siswa_kelas	1
12	2019_08_19_122630_perpustakaan	1
13	2019_08_19_122632_gls	1
14	2019_08_19_123250_ijin	1
15	2019_08_19_123828_absensi	1
16	2019_08_19_124113_log_sistem	1
17	2019_08_19_124453_opsi_penilaian_karakter	1
18	2019_08_19_124722_penilaian_karakter	1
19	2019_08_19_125301_opsi_pelanggaran	1
20	2019_08_19_125302_pelanggaran_siswa	1
21	2019_09_01_220520_setting_admin	1
124	2019_08_28_124350_add_barcode__perpustakaan	2
125	2019_08_28_124914_add_barcode__perpustakaan	3
110	2014_10_12_000000_create_users_table	1
111	2014_10_12_100000_create_password_resets_table	1
112	2019_08_19_121643_siswa	1
113	2019_08_19_121644_kelas	1
114	2019_08_19_122307_siswa_kelas	1
115	2019_08_19_122630_perpustakaan	1
116	2019_08_19_122631_gls	1
117	2019_08_19_123250_ijin	1
118	2019_08_19_123828_absensi	1
119	2019_08_19_124113_log_sistem	1
120	2019_08_19_124453_opsi_penilaian_karakter	1
121	2019_08_19_124722_penilaian_karakter	1
122	2019_08_19_125301_opsi_pelanggaran	1
123	2019_08_19_125302_pelanggaran_siswa	1
\.


--
-- Data for Name: oauth_access_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_access_tokens (id, user_id, client_id, name, scopes, revoked, created_at, updated_at, expires_at) FROM stdin;
\.


--
-- Data for Name: oauth_auth_codes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_auth_codes (id, user_id, client_id, scopes, revoked, expires_at) FROM stdin;
\.


--
-- Data for Name: oauth_clients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_clients (id, user_id, name, secret, redirect, personal_access_client, password_client, revoked, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: oauth_personal_access_clients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_personal_access_clients (id, client_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: oauth_refresh_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_refresh_tokens (id, access_token_id, revoked, expires_at) FROM stdin;
\.


--
-- Data for Name: opsi_pelanggaran; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.opsi_pelanggaran (id, id_admin, nama_pelanggaran, bobot_pelanggaran, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: opsi_penilaian_karakter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.opsi_penilaian_karakter (id, opsi_tema_penilaian, opsi_konten, opsi_bobot_nilai, id_admin, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: pelanggaran_siswa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pelanggaran_siswa (id, id_siswa, id_opsi_pelanggaran, id_admin, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: penilaian_karakter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.penilaian_karakter (id, id_siswa, id_opsi_penilaian_karakter, id_admin, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: perpustakaan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.perpustakaan (id, id_admin, perpustakaan_barcode, perpustakaan_isbn, perpustakaan_judul_buku, perpustakaan_pengarang, perpustakaan_penerbit, perpustakaan_kode_buku, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: sekolah; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sekolah (id, nama_sekolah, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: setting_admin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.setting_admin (id, id_admin, setting_title, setting_value, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: siswa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.siswa (id, id_sekolah, siswa_nis, siswa_nama, siswa_alamat, siswa_foto, created_at, updated_at) FROM stdin;
1	\N	123456	\N	\N	images/siswa_photo/photo_123456_1566440280.jpg	2019-08-22 02:18:05	2019-08-22 02:18:05
2	\N	12345678	\N	\N	images/siswa_photo/photo_12345678_1566440317.jpg	2019-08-22 02:18:41	2019-08-22 02:18:41
3	\N	123456789	\N	\N	images/siswa_photo/photo_123456789_1566440364.jpg	2019-08-22 02:19:28	2019-08-22 02:19:28
6	\N	3967	\N	\N	images/siswa_photo/photo_3967_1566441147.jpg	2019-08-22 02:32:31	2019-08-22 02:32:31
8	\N	0118366757	\N	\N	images/siswa_photo/photo_0118366757_1566441697.jpg	2019-08-22 02:41:41	2019-08-22 02:41:41
9	\N	01	\N	\N	images/siswa_photo/photo_01_1566441809.jpg	2019-08-22 02:43:33	2019-08-22 02:43:33
10	\N	0123187019	\N	\N	images/siswa_photo/photo_0123187019_1566441929.jpg	2019-08-22 02:45:33	2019-08-22 02:45:33
11	\N	0128045801	\N	\N	images/siswa_photo/photo_0128045801_1566441973.jpg	2019-08-22 02:46:17	2019-08-22 02:46:17
12	\N	0135546885	\N	\N	images/siswa_photo/photo_0135546885_1566442032.jpg	2019-08-22 02:47:16	2019-08-22 02:47:16
13	\N	02	\N	\N	images/siswa_photo/photo_02_1566442107.jpg	2019-08-22 02:48:29	2019-08-22 02:48:29
14	\N	0128426874	\N	\N	images/siswa_photo/photo_0128426874_1566442161.jpg	2019-08-22 02:49:24	2019-08-22 02:49:24
15	\N	03	\N	\N	images/siswa_photo/photo_03_1566442235.jpg	2019-08-22 02:50:37	2019-08-22 02:50:37
16	\N	0121106079	\N	\N	images/siswa_photo/photo_0121106079_1566442326.jpg	2019-08-22 02:52:08	2019-08-22 02:52:08
17	\N	04	\N	\N	images/siswa_photo/photo_04_1566442379.jpg	2019-08-22 02:53:01	2019-08-22 02:53:01
18	\N	0136701054	\N	\N	images/siswa_photo/photo_0136701054_1566442434.jpg	2019-08-22 02:53:56	2019-08-22 02:53:56
19	\N	05	\N	\N	images/siswa_photo/photo_05_1566442518.jpg	2019-08-22 02:55:20	2019-08-22 02:55:20
20	\N	06	\N	\N	images/siswa_photo/photo_06_1566442595.jpg	2019-08-22 02:56:37	2019-08-22 02:56:37
21	\N	0137704302	\N	\N	images/siswa_photo/photo_0137704302_1566442638.jpg	2019-08-22 02:57:20	2019-08-22 02:57:20
22	\N	0112128741	\N	\N	images/siswa_photo/photo_0112128741_1566442710.jpg	2019-08-22 02:58:32	2019-08-22 02:58:32
23	\N	0122217952	\N	\N	images/siswa_photo/photo_0122217952_1566442794.jpg	2019-08-22 02:59:56	2019-08-22 02:59:56
24	\N	07	\N	\N	images/siswa_photo/photo_07_1566442836.jpg	2019-08-22 03:00:38	2019-08-22 03:00:38
25	\N	08	\N	\N	images/siswa_photo/photo_08_1566442881.jpg	2019-08-22 03:01:24	2019-08-22 03:01:24
26	\N	09	\N	\N	images/siswa_photo/photo_09_1566442945.jpg	2019-08-22 03:02:27	2019-08-22 03:02:27
27	\N	10	\N	\N	images/siswa_photo/photo_10_1566443091.jpg	2019-08-22 03:04:53	2019-08-22 03:04:53
28	\N	11	\N	\N	images/siswa_photo/photo_11_1566443163.jpg	2019-08-22 03:06:06	2019-08-22 03:06:06
29	\N	12	\N	\N	images/siswa_photo/photo_12_1566443252.jpg	2019-08-22 03:07:34	2019-08-22 03:07:34
30	\N	0129167478	\N	\N	images/siswa_photo/photo_0129167478_1566443335.jpg	2019-08-22 03:08:57	2019-08-22 03:08:57
31	\N	13	\N	\N	images/siswa_photo/photo_13_1566443378.jpg	2019-08-22 03:09:41	2019-08-22 03:09:41
32	\N	14	\N	\N	images/siswa_photo/photo_14_1566443460.jpg	2019-08-22 03:11:02	2019-08-22 03:11:02
33	\N	0135068570	\N	\N	images/siswa_photo/photo_0135068570_1566443543.jpg	2019-08-22 03:12:25	2019-08-22 03:12:25
34	\N	0123638269	\N	\N	images/siswa_photo/photo_0123638269_1566443594.jpg	2019-08-22 03:13:16	2019-08-22 03:13:16
35	\N	0111124563	\N	\N	images/siswa_photo/photo_0111124563_1566443682.jpg	2019-08-22 03:14:45	2019-08-22 03:14:45
36	\N	0121972674	\N	\N	images/siswa_photo/photo_0121972674_1566443798.jpg	2019-08-22 03:16:41	2019-08-22 03:16:41
37	\N	0125561164	\N	\N	images/siswa_photo/photo_0125561164_1566443885.jpg	2019-08-22 03:18:07	2019-08-22 03:18:07
38	\N	0114094764	\N	\N	images/siswa_photo/photo_0114094764_1566443968.jpg	2019-08-22 03:19:30	2019-08-22 03:19:30
39	\N	0112202060	\N	\N	images/siswa_photo/photo_0112202060_1566444076.jpg	2019-08-22 03:21:18	2019-08-22 03:21:18
40	\N	0123525698	\N	\N	images/siswa_photo/photo_0123525698_1566444153.jpg	2019-08-22 03:22:35	2019-08-22 03:22:35
41	\N	0113617198	\N	\N	images/siswa_photo/photo_0113617198_1566444249.jpg	2019-08-22 03:24:12	2019-08-22 03:24:12
42	\N	0125028450	\N	\N	images/siswa_photo/photo_0125028450_1566444287.jpg	2019-08-22 03:24:49	2019-08-22 03:24:49
43	\N	0112881519	\N	\N	images/siswa_photo/photo_0112881519_1566444373.jpg	2019-08-22 03:26:15	2019-08-22 03:26:15
44	\N	0116718470	\N	\N	images/siswa_photo/photo_0116718470_1566444421.jpg	2019-08-22 03:27:03	2019-08-22 03:27:03
45	\N	0119404316	\N	\N	images/siswa_photo/photo_0119404316_1566444478.jpg	2019-08-22 03:28:00	2019-08-22 03:28:00
46	\N	0118687007	\N	\N	images/siswa_photo/photo_0118687007_1566444557.jpg	2019-08-22 03:29:19	2019-08-22 03:29:19
47	\N	0123466118	\N	\N	images/siswa_photo/photo_0123466118_1566444636.jpg	2019-08-22 03:30:38	2019-08-22 03:30:38
48	\N	0122842457	\N	\N	images/siswa_photo/photo_0122842457_1566444701.jpg	2019-08-22 03:31:43	2019-08-22 03:31:43
49	\N	0115563667	\N	\N	images/siswa_photo/photo_0115563667_1566444756.jpg	2019-08-22 03:32:38	2019-08-22 03:32:38
50	\N	0108653531	\N	\N	images/siswa_photo/photo_0108653531_1566444882.jpg	2019-08-22 03:34:44	2019-08-22 03:34:44
51	\N	0101482551	\N	\N	images/siswa_photo/photo_0101482551_1566444959.jpg	2019-08-22 03:36:01	2019-08-22 03:36:01
52	\N	0115654330	\N	\N	images/siswa_photo/photo_0115654330_1566445035.jpg	2019-08-22 03:37:17	2019-08-22 03:37:17
53	\N	0116209826	\N	\N	images/siswa_photo/photo_0116209826_1566445126.jpg	2019-08-22 03:38:48	2019-08-22 03:38:48
54	\N	0112847565	\N	\N	images/siswa_photo/photo_0112847565_1566445223.jpg	2019-08-22 03:40:25	2019-08-22 03:40:25
55	\N	0128814182	\N	\N	images/siswa_photo/photo_0128814182_1566445308.jpg	2019-08-22 03:41:50	2019-08-22 03:41:50
56	\N	0123765029	\N	\N	images/siswa_photo/photo_0123765029_1566445372.jpg	2019-08-22 03:42:54	2019-08-22 03:42:54
57	\N	0117562694	\N	\N	images/siswa_photo/photo_0117562694_1566445421.jpg	2019-08-22 03:43:43	2019-08-22 03:43:43
58	\N	0117711678	\N	\N	images/siswa_photo/photo_0117711678_1566445528.jpg	2019-08-22 03:45:31	2019-08-22 03:45:31
59	\N	0114930968	\N	\N	images/siswa_photo/photo_0114930968_1566445607.jpg	2019-08-22 03:46:49	2019-08-22 03:46:49
60	\N	0116437118	\N	\N	images/siswa_photo/photo_0116437118_1566445707.jpg	2019-08-22 03:48:29	2019-08-22 03:48:29
61	\N	0112846106	\N	\N	images/siswa_photo/photo_0112846106_1566445774.jpg	2019-08-22 03:49:36	2019-08-22 03:49:36
62	\N	0116310263	\N	\N	images/siswa_photo/photo_0116310263_1566445824.jpg	2019-08-22 03:50:26	2019-08-22 03:50:26
63	\N	0114552042	\N	\N	images/siswa_photo/photo_0114552042_1566445882.jpg	2019-08-22 03:51:24	2019-08-22 03:51:24
64	\N	0118397142	\N	\N	images/siswa_photo/photo_0118397142_1566445942.jpg	2019-08-22 03:52:24	2019-08-22 03:52:24
65	\N	0104745442	\N	\N	images/siswa_photo/photo_0104745442_1566446140.jpg	2019-08-22 03:55:42	2019-08-22 03:55:42
66	\N	0115322130	\N	\N	images/siswa_photo/photo_0115322130_1566446230.jpg	2019-08-22 03:57:12	2019-08-22 03:57:12
67	\N	0106643894	\N	\N	images/siswa_photo/photo_0106643894_1566446301.jpg	2019-08-22 03:58:24	2019-08-22 03:58:24
68	\N	0107903533	\N	\N	images/siswa_photo/photo_0107903533_1566446385.jpg	2019-08-22 03:59:47	2019-08-22 03:59:47
69	\N	0114577974	\N	\N	images/siswa_photo/photo_0114577974_1566446498.jpg	2019-08-22 04:01:41	2019-08-22 04:01:41
70	\N	0111318277	\N	\N	images/siswa_photo/photo_0111318277_1566446606.jpg	2019-08-22 04:03:29	2019-08-22 04:03:29
71	\N	0107031899	\N	\N	images/siswa_photo/photo_0107031899_1566446791.jpg	2019-08-22 04:06:33	2019-08-22 04:06:33
72	\N	0101389756	\N	\N	images/siswa_photo/photo_0101389756_1566446888.jpg	2019-08-22 04:08:10	2019-08-22 04:08:10
73	\N	01077173198	\N	\N	images/siswa_photo/photo_01077173198_1566446985.jpg	2019-08-22 04:09:47	2019-08-22 04:09:47
4	\N	258010495	nama siswa	alamat	images/siswa_photo/photo__1566834362.jpg	2019-08-22 02:20:10	2019-08-26 15:46:03
74	\N	0104476192	\N	\N	images/siswa_photo/photo_0104476192_1566447043.jpg	2019-08-22 04:10:45	2019-08-22 04:10:45
75	\N	0114127782	\N	\N	images/siswa_photo/photo_0114127782_1566447105.jpg	2019-08-22 04:11:47	2019-08-22 04:11:47
76	\N	0103920619	\N	\N	images/siswa_photo/photo_0103920619_1566447209.jpg	2019-08-22 04:13:31	2019-08-22 04:13:31
77	\N	0102127824	\N	\N	images/siswa_photo/photo_0102127824_1566447296.jpg	2019-08-22 04:14:58	2019-08-22 04:14:58
78	\N	0109891179	\N	\N	images/siswa_photo/photo_0109891179_1566447389.jpg	2019-08-22 04:16:31	2019-08-22 04:16:31
79	\N	0108862816	\N	\N	images/siswa_photo/photo_0108862816_1566447489.jpg	2019-08-22 04:18:11	2019-08-22 04:18:11
80	\N	0119638464	\N	\N	images/siswa_photo/photo_0119638464_1566447556.jpg	2019-08-22 04:19:18	2019-08-22 04:19:18
81	\N	0103130738	\N	\N	images/siswa_photo/photo_0103130738_1566447630.jpg	2019-08-22 04:20:32	2019-08-22 04:20:32
82	\N	0102285096	\N	\N	images/siswa_photo/photo_0102285096_1566447676.jpg	2019-08-22 04:21:18	2019-08-22 04:21:18
83	\N	0108019641	\N	\N	images/siswa_photo/photo_0108019641_1566447754.jpg	2019-08-22 04:22:36	2019-08-22 04:22:36
84	\N	0112665015	\N	\N	images/siswa_photo/photo_0112665015_1566447851.jpg	2019-08-22 04:24:13	2019-08-22 04:24:13
85	\N	0105553222	\N	\N	images/siswa_photo/photo_0105553222_1566447933.jpg	2019-08-22 04:25:36	2019-08-22 04:25:36
86	\N	0105670732	\N	\N	images/siswa_photo/photo_0105670732_1566447990.jpg	2019-08-22 04:26:32	2019-08-22 04:26:32
87	\N	0112258793	\N	\N	images/siswa_photo/photo_0112258793_1566448044.jpg	2019-08-22 04:27:26	2019-08-22 04:27:26
88	\N	0114778427	\N	\N	images/siswa_photo/photo_0114778427_1566448166.jpg	2019-08-22 04:29:28	2019-08-22 04:29:28
89	\N	0116152556	\N	\N	images/siswa_photo/photo_0116152556_1566448232.jpg	2019-08-22 04:30:34	2019-08-22 04:30:34
90	\N	0091475884	\N	\N	images/siswa_photo/photo_0091475884_1566448305.jpg	2019-08-22 04:31:47	2019-08-22 04:31:47
91	\N	0092186514	\N	\N	images/siswa_photo/photo_0092186514_1566448355.jpg	2019-08-22 04:32:37	2019-08-22 04:32:37
92	\N	0092396001	\N	\N	images/siswa_photo/photo_0092396001_1566448430.jpg	2019-08-22 04:33:52	2019-08-22 04:33:52
93	\N	0095898163	\N	\N	images/siswa_photo/photo_0095898163_1566448474.jpg	2019-08-22 04:34:36	2019-08-22 04:34:36
94	\N	0094511859	\N	\N	images/siswa_photo/photo_0094511859_1566448577.jpg	2019-08-22 04:36:19	2019-08-22 04:36:19
95	\N	0096275873	\N	\N	images/siswa_photo/photo_0096275873_1566448618.jpg	2019-08-22 04:37:00	2019-08-22 04:37:00
96	\N	0105693607	\N	\N	images/siswa_photo/photo_0105693607_1566448678.jpg	2019-08-22 04:38:00	2019-08-22 04:38:00
97	\N	0091446186	\N	\N	images/siswa_photo/photo_0091446186_1566448721.jpg	2019-08-22 04:38:43	2019-08-22 04:38:43
98	\N	0097233092	\N	\N	images/siswa_photo/photo_0097233092_1566448759.jpg	2019-08-22 04:39:21	2019-08-22 04:39:21
99	\N	0094930293	\N	\N	images/siswa_photo/photo_0094930293_1566448819.jpg	2019-08-22 04:40:21	2019-08-22 04:40:21
100	\N	0091036481	\N	\N	images/siswa_photo/photo_0091036481_1566448873.jpg	2019-08-22 04:41:15	2019-08-22 04:41:15
101	\N	0094077835	\N	\N	images/siswa_photo/photo_0094077835_1566448970.jpg	2019-08-22 04:42:52	2019-08-22 04:42:52
102	\N	0094499087	\N	\N	images/siswa_photo/photo_0094499087_1566449014.jpg	2019-08-22 04:43:36	2019-08-22 04:43:36
103	\N	0094180749	\N	\N	images/siswa_photo/photo_0094180749_1566449124.jpg	2019-08-22 04:45:26	2019-08-22 04:45:26
104	\N	0107515479	\N	\N	images/siswa_photo/photo_0107515479_1566449229.jpg	2019-08-22 04:47:11	2019-08-22 04:47:11
105	\N	0092615326	\N	\N	images/siswa_photo/photo_0092615326_1566449292.jpg	2019-08-22 04:48:14	2019-08-22 04:48:14
106	\N	0096493079	\N	\N	images/siswa_photo/photo_0096493079_1566449348.jpg	2019-08-22 04:49:10	2019-08-22 04:49:10
107	\N	0095404759	\N	\N	images/siswa_photo/photo_0095404759_1566449414.jpg	2019-08-22 04:50:16	2019-08-22 04:50:16
108	\N	0098427597	\N	\N	images/siswa_photo/photo_0098427597_1566449504.jpg	2019-08-22 04:51:46	2019-08-22 04:51:46
109	\N	0108795331	\N	\N	images/siswa_photo/photo_0108795331_1566449549.jpg	2019-08-22 04:52:31	2019-08-22 04:52:31
110	\N	0082377998	\N	\N	images/siswa_photo/photo_0082377998_1566449604.jpg	2019-08-22 04:53:26	2019-08-22 04:53:26
111	\N	0091123726	\N	\N	images/siswa_photo/photo_0091123726_1566449669.jpg	2019-08-22 04:54:31	2019-08-22 04:54:31
112	\N	0099542858	\N	\N	images/siswa_photo/photo_0099542858_1566449712.jpg	2019-08-22 04:55:14	2019-08-22 04:55:14
113	\N	0096044175	\N	\N	images/siswa_photo/photo_0096044175_1566449775.jpg	2019-08-22 04:56:17	2019-08-22 04:56:17
114	\N	0096102115	\N	\N	images/siswa_photo/photo_0096102115_1566449828.jpg	2019-08-22 04:57:10	2019-08-22 04:57:10
115	\N	0103976479	\N	\N	images/siswa_photo/photo_0103976479_1566449906.jpg	2019-08-22 04:58:28	2019-08-22 04:58:28
116	\N	0099720989	\N	\N	images/siswa_photo/photo_0099720989_1566449959.jpg	2019-08-22 04:59:21	2019-08-22 04:59:21
117	\N	0095833324	\N	\N	images/siswa_photo/photo_0095833324_1566450002.jpg	2019-08-22 05:00:04	2019-08-22 05:00:04
118	\N	0062021815	\N	\N	images/siswa_photo/photo_0062021815_1566450510.jpg	2019-08-22 05:08:32	2019-08-22 05:08:32
119	\N	0079018175	\N	\N	images/siswa_photo/photo_0079018175_1566450569.jpg	2019-08-22 05:09:31	2019-08-22 05:09:31
120	\N	0084895463	\N	\N	images/siswa_photo/photo_0084895463_1566450628.jpg	2019-08-22 05:10:30	2019-08-22 05:10:30
121	\N	0081844636	\N	\N	images/siswa_photo/photo_0081844636_1566450699.jpg	2019-08-22 05:11:41	2019-08-22 05:11:41
122	\N	0094638152	\N	\N	images/siswa_photo/photo_0094638152_1566450738.jpg	2019-08-22 05:12:21	2019-08-22 05:12:21
123	\N	0105064688	\N	\N	images/siswa_photo/photo_0105064688_1566450803.jpg	2019-08-22 05:13:26	2019-08-22 05:13:26
124	\N	0082375980	\N	\N	images/siswa_photo/photo_0082375980_1566450930.jpg	2019-08-22 05:15:32	2019-08-22 05:15:32
125	\N	0086301685	\N	\N	images/siswa_photo/photo_0086301685_1566450997.jpg	2019-08-22 05:16:39	2019-08-22 05:16:39
126	\N	0093048195	\N	\N	images/siswa_photo/photo_0093048195_1566451093.jpg	2019-08-22 05:18:16	2019-08-22 05:18:16
127	\N	0085594952	\N	\N	images/siswa_photo/photo_0085594952_1566451169.jpg	2019-08-22 05:19:31	2019-08-22 05:19:31
128	\N	0083626033	\N	\N	images/siswa_photo/photo_0083626033_1566451223.jpg	2019-08-22 05:20:25	2019-08-22 05:20:25
129	\N	0086093207	\N	\N	images/siswa_photo/photo_0086093207_1566451284.jpg	2019-08-22 05:21:26	2019-08-22 05:21:26
130	\N	0086136994	\N	\N	images/siswa_photo/photo_0086136994_1566451366.jpg	2019-08-22 05:22:48	2019-08-22 05:22:48
131	\N	0085309824	\N	\N	images/siswa_photo/photo_0085309824_1566451409.jpg	2019-08-22 05:23:31	2019-08-22 05:23:31
132	\N	0082808322	\N	\N	images/siswa_photo/photo_0082808322_1566451465.jpg	2019-08-22 05:24:27	2019-08-22 05:24:27
133	\N	0092958035	\N	\N	images/siswa_photo/photo_0092958035_1566451519.jpg	2019-08-22 05:25:21	2019-08-22 05:25:21
134	\N	0088013276	\N	\N	images/siswa_photo/photo_0088013276_1566451596.jpg	2019-08-22 05:26:38	2019-08-22 05:26:38
135	\N	0087382904	\N	\N	images/siswa_photo/photo_0087382904_1566451689.jpg	2019-08-22 05:28:11	2019-08-22 05:28:11
136	\N	0089771934	\N	\N	images/siswa_photo/photo_0089771934_1566451746.jpg	2019-08-22 05:29:09	2019-08-22 05:29:09
137	\N	0098728686	\N	\N	images/siswa_photo/photo_0098728686_1566451783.jpg	2019-08-22 05:29:45	2019-08-22 05:29:45
138	\N	0088694018	\N	\N	images/siswa_photo/photo_0088694018_1566451826.jpg	2019-08-22 05:30:28	2019-08-22 05:30:28
139	\N	0082825377	\N	\N	images/siswa_photo/photo_0082825377_1566451899.jpg	2019-08-22 05:31:42	2019-08-22 05:31:42
140	\N	0087745272	\N	\N	images/siswa_photo/photo_0087745272_1566451949.jpg	2019-08-22 05:32:32	2019-08-22 05:32:32
141	\N	0083557711	\N	\N	images/siswa_photo/photo_0083557711_1566452018.jpg	2019-08-22 05:33:40	2019-08-22 05:33:40
142	\N	0085610630	\N	\N	images/siswa_photo/photo_0085610630_1566452104.jpg	2019-08-22 05:35:06	2019-08-22 05:35:06
143	\N	0086318336	\N	\N	images/siswa_photo/photo_0086318336_1566452150.jpg	2019-08-22 05:35:52	2019-08-22 05:35:52
144	\N	0085331777	\N	\N	images/siswa_photo/photo_0085331777_1566452221.jpg	2019-08-22 05:37:03	2019-08-22 05:37:03
145	\N	0099843215	\N	\N	images/siswa_photo/photo_0099843215_1566452273.jpg	2019-08-22 05:37:55	2019-08-22 05:37:55
146	\N	0085370865	\N	\N	images/siswa_photo/photo_0085370865_1566452327.jpg	2019-08-22 05:38:49	2019-08-22 05:38:49
147	\N	0092018666	\N	\N	images/siswa_photo/photo_0092018666_1566452470.jpg	2019-08-22 05:41:13	2019-08-22 05:41:13
148	\N	0091672587	\N	\N	images/siswa_photo/photo_0091672587_1566452530.jpg	2019-08-22 05:42:12	2019-08-22 05:42:12
149	\N	0081512862	\N	\N	images/siswa_photo/photo_0081512862_1566452594.jpg	2019-08-22 05:43:16	2019-08-22 05:43:16
150	\N	0088910346	\N	\N	images/siswa_photo/photo_0088910346_1566452635.jpg	2019-08-22 05:43:57	2019-08-22 05:43:57
151	\N	0086660000	\N	\N	images/siswa_photo/photo_0086660000_1566452679.jpg	2019-08-22 05:44:42	2019-08-22 05:44:42
152	\N	0098781744	\N	\N	images/siswa_photo/photo_0098781744_1566452749.jpg	2019-08-22 05:45:51	2019-08-22 05:45:51
153	\N	0086901901	\N	\N	images/siswa_photo/photo_0086901901_1566452800.jpg	2019-08-22 05:46:42	2019-08-22 05:46:42
154	\N	0088937726	\N	\N	images/siswa_photo/photo_0088937726_1566452846.jpg	2019-08-22 05:47:28	2019-08-22 05:47:28
155	\N	0085282541	\N	\N	images/siswa_photo/photo_0085282541_1566452889.jpg	2019-08-22 05:48:11	2019-08-22 05:48:11
156	\N	0078905613	\N	\N	images/siswa_photo/photo_0078905613_1566452947.jpg	2019-08-22 05:49:09	2019-08-22 05:49:09
157	\N	0079476057	\N	\N	images/siswa_photo/photo_0079476057_1566453029.jpg	2019-08-22 05:50:31	2019-08-22 05:50:31
158	\N	0079876473	\N	\N	images/siswa_photo/photo_0079876473_1566453081.jpg	2019-08-22 05:51:23	2019-08-22 05:51:23
159	\N	0089605785	\N	\N	images/siswa_photo/photo_0089605785_1566453129.jpg	2019-08-22 05:52:12	2019-08-22 05:52:12
160	\N	0084482466	\N	\N	images/siswa_photo/photo_0084482466_1566453170.jpg	2019-08-22 05:52:52	2019-08-22 05:52:52
161	\N	0083643854	\N	\N	images/siswa_photo/photo_0083643854_1566453215.jpg	2019-08-22 05:53:37	2019-08-22 05:53:37
162	\N	0072885910	\N	\N	images/siswa_photo/photo_0072885910_1566453252.jpg	2019-08-22 05:54:14	2019-08-22 05:54:14
163	\N	0072082021	\N	\N	images/siswa_photo/photo_0072082021_1566453296.jpg	2019-08-22 05:54:58	2019-08-22 05:54:58
164	\N	0072322157	\N	\N	images/siswa_photo/photo_0072322157_1566453343.jpg	2019-08-22 05:55:46	2019-08-22 05:55:46
165	\N	0078548059	\N	\N	images/siswa_photo/photo_0078548059_1566453400.jpg	2019-08-22 05:56:43	2019-08-22 05:56:43
166	\N	0072181442	\N	\N	images/siswa_photo/photo_0072181442_1566453465.jpg	2019-08-22 05:57:47	2019-08-22 05:57:47
167	\N	0076389836	\N	\N	images/siswa_photo/photo_0076389836_1566453517.jpg	2019-08-22 05:58:39	2019-08-22 05:58:39
168	\N	0077092335	\N	\N	images/siswa_photo/photo_0077092335_1566453547.jpg	2019-08-22 05:59:09	2019-08-22 05:59:09
169	\N	0078183935	\N	\N	images/siswa_photo/photo_0078183935_1566453607.jpg	2019-08-22 06:00:09	2019-08-22 06:00:09
170	\N	0079127919	\N	\N	images/siswa_photo/photo_0079127919_1566453647.jpg	2019-08-22 06:00:50	2019-08-22 06:00:50
171	\N	0079866349	\N	\N	images/siswa_photo/photo_0079866349_1566453696.jpg	2019-08-22 06:01:39	2019-08-22 06:01:39
172	\N	0074156383	\N	\N	images/siswa_photo/photo_0074156383_1566453734.jpg	2019-08-22 06:02:16	2019-08-22 06:02:16
173	\N	0074750007	\N	\N	images/siswa_photo/photo_0074750007_1566453808.jpg	2019-08-22 06:03:30	2019-08-22 06:03:30
174	\N	0076385562	\N	\N	images/siswa_photo/photo_0076385562_1566453884.jpg	2019-08-22 06:04:46	2019-08-22 06:04:46
175	\N	0083592387	\N	\N	images/siswa_photo/photo_0083592387_1566453925.jpg	2019-08-22 06:05:27	2019-08-22 06:05:27
176	\N	0073016755	\N	\N	images/siswa_photo/photo_0073016755_1566453961.jpg	2019-08-22 06:06:03	2019-08-22 06:06:03
177	\N	0083484325	\N	\N	images/siswa_photo/photo_0083484325_1566454017.jpg	2019-08-22 06:06:59	2019-08-22 06:06:59
178	\N	0072487517	\N	\N	images/siswa_photo/photo_0072487517_1566454060.jpg	2019-08-22 06:07:42	2019-08-22 06:07:42
179	\N	0082919857	\N	\N	images/siswa_photo/photo_0082919857_1566454144.jpg	2019-08-22 06:09:06	2019-08-22 06:09:06
180	\N	0086422670	\N	\N	images/siswa_photo/photo_0086422670_1566454206.jpg	2019-08-22 06:10:09	2019-08-22 06:10:09
181	\N	123	123	123	images/siswa_photo/photo__1566834935.png	2019-08-26 15:55:36	2019-08-26 15:55:36
\.


--
-- Data for Name: siswa_kelas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.siswa_kelas (id, id_siswa, id_kelas, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, type, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
\.


--
-- Name: absensi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.absensi_id_seq', 1, false);


--
-- Name: gls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gls_id_seq', 1, false);


--
-- Name: ijin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ijin_id_seq', 1, false);


--
-- Name: kelas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.kelas_id_seq', 1, false);


--
-- Name: log_sistem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_sistem_id_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 125, true);


--
-- Name: oauth_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oauth_clients_id_seq', 1, false);


--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oauth_personal_access_clients_id_seq', 1, false);


--
-- Name: opsi_pelanggaran_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.opsi_pelanggaran_id_seq', 1, false);


--
-- Name: opsi_penilaian_karakter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.opsi_penilaian_karakter_id_seq', 1, false);


--
-- Name: pelanggaran_siswa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pelanggaran_siswa_id_seq', 1, false);


--
-- Name: penilaian_karakter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.penilaian_karakter_id_seq', 1, false);


--
-- Name: perpustakaan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.perpustakaan_id_seq', 1, false);


--
-- Name: sekolah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sekolah_id_seq', 1, false);


--
-- Name: setting_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.setting_admin_id_seq', 1, false);


--
-- Name: siswa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.siswa_id_seq', 181, true);


--
-- Name: siswa_kelas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.siswa_kelas_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- Name: absensi absensi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.absensi
    ADD CONSTRAINT absensi_pkey PRIMARY KEY (id);


--
-- Name: gls gls_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gls
    ADD CONSTRAINT gls_pkey PRIMARY KEY (id);


--
-- Name: ijin ijin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ijin
    ADD CONSTRAINT ijin_pkey PRIMARY KEY (id);


--
-- Name: kelas kelas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kelas
    ADD CONSTRAINT kelas_pkey PRIMARY KEY (id);


--
-- Name: log_sistem log_sistem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.log_sistem
    ADD CONSTRAINT log_sistem_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_tokens oauth_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: oauth_auth_codes oauth_auth_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_auth_codes
    ADD CONSTRAINT oauth_auth_codes_pkey PRIMARY KEY (id);


--
-- Name: oauth_clients oauth_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_clients
    ADD CONSTRAINT oauth_clients_pkey PRIMARY KEY (id);


--
-- Name: oauth_personal_access_clients oauth_personal_access_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_personal_access_clients
    ADD CONSTRAINT oauth_personal_access_clients_pkey PRIMARY KEY (id);


--
-- Name: oauth_refresh_tokens oauth_refresh_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_refresh_tokens
    ADD CONSTRAINT oauth_refresh_tokens_pkey PRIMARY KEY (id);


--
-- Name: opsi_pelanggaran opsi_pelanggaran_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opsi_pelanggaran
    ADD CONSTRAINT opsi_pelanggaran_pkey PRIMARY KEY (id);


--
-- Name: opsi_penilaian_karakter opsi_penilaian_karakter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opsi_penilaian_karakter
    ADD CONSTRAINT opsi_penilaian_karakter_pkey PRIMARY KEY (id);


--
-- Name: pelanggaran_siswa pelanggaran_siswa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pelanggaran_siswa
    ADD CONSTRAINT pelanggaran_siswa_pkey PRIMARY KEY (id);


--
-- Name: penilaian_karakter penilaian_karakter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.penilaian_karakter
    ADD CONSTRAINT penilaian_karakter_pkey PRIMARY KEY (id);


--
-- Name: perpustakaan perpustakaan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perpustakaan
    ADD CONSTRAINT perpustakaan_pkey PRIMARY KEY (id);


--
-- Name: sekolah sekolah_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sekolah
    ADD CONSTRAINT sekolah_pkey PRIMARY KEY (id);


--
-- Name: setting_admin setting_admin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.setting_admin
    ADD CONSTRAINT setting_admin_pkey PRIMARY KEY (id);


--
-- Name: siswa_kelas siswa_kelas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siswa_kelas
    ADD CONSTRAINT siswa_kelas_pkey PRIMARY KEY (id);


--
-- Name: siswa siswa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siswa
    ADD CONSTRAINT siswa_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_tokens_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_access_tokens_user_id_index ON public.oauth_access_tokens USING btree (user_id);


--
-- Name: oauth_clients_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_clients_user_id_index ON public.oauth_clients USING btree (user_id);


--
-- Name: oauth_personal_access_clients_client_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_personal_access_clients_client_id_index ON public.oauth_personal_access_clients USING btree (client_id);


--
-- Name: oauth_refresh_tokens_access_token_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_refresh_tokens_access_token_id_index ON public.oauth_refresh_tokens USING btree (access_token_id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: absensi absensi_id_siswa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.absensi
    ADD CONSTRAINT absensi_id_siswa_foreign FOREIGN KEY (id_siswa) REFERENCES public.siswa(id);


--
-- Name: gls gls_id_buku_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gls
    ADD CONSTRAINT gls_id_buku_foreign FOREIGN KEY (id_buku) REFERENCES public.perpustakaan(id);


--
-- Name: gls gls_id_siswa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gls
    ADD CONSTRAINT gls_id_siswa_foreign FOREIGN KEY (id_siswa) REFERENCES public.siswa(id);


--
-- Name: ijin ijin_id_siswa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ijin
    ADD CONSTRAINT ijin_id_siswa_foreign FOREIGN KEY (id_siswa) REFERENCES public.siswa(id);


--
-- Name: opsi_pelanggaran opsi_pelanggaran_id_admin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opsi_pelanggaran
    ADD CONSTRAINT opsi_pelanggaran_id_admin_foreign FOREIGN KEY (id_admin) REFERENCES public.users(id);


--
-- Name: opsi_penilaian_karakter opsi_penilaian_karakter_id_admin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opsi_penilaian_karakter
    ADD CONSTRAINT opsi_penilaian_karakter_id_admin_foreign FOREIGN KEY (id_admin) REFERENCES public.users(id);


--
-- Name: pelanggaran_siswa pelanggaran_siswa_id_admin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pelanggaran_siswa
    ADD CONSTRAINT pelanggaran_siswa_id_admin_foreign FOREIGN KEY (id_admin) REFERENCES public.users(id);


--
-- Name: pelanggaran_siswa pelanggaran_siswa_id_opsi_pelanggaran_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pelanggaran_siswa
    ADD CONSTRAINT pelanggaran_siswa_id_opsi_pelanggaran_foreign FOREIGN KEY (id_opsi_pelanggaran) REFERENCES public.opsi_pelanggaran(id);


--
-- Name: pelanggaran_siswa pelanggaran_siswa_id_siswa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pelanggaran_siswa
    ADD CONSTRAINT pelanggaran_siswa_id_siswa_foreign FOREIGN KEY (id_siswa) REFERENCES public.siswa(id);


--
-- Name: penilaian_karakter penilaian_karakter_id_admin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.penilaian_karakter
    ADD CONSTRAINT penilaian_karakter_id_admin_foreign FOREIGN KEY (id_admin) REFERENCES public.users(id);


--
-- Name: penilaian_karakter penilaian_karakter_id_opsi_penilaian_karakter_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.penilaian_karakter
    ADD CONSTRAINT penilaian_karakter_id_opsi_penilaian_karakter_foreign FOREIGN KEY (id_opsi_penilaian_karakter) REFERENCES public.opsi_penilaian_karakter(id);


--
-- Name: penilaian_karakter penilaian_karakter_id_siswa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.penilaian_karakter
    ADD CONSTRAINT penilaian_karakter_id_siswa_foreign FOREIGN KEY (id_siswa) REFERENCES public.siswa(id);


--
-- Name: perpustakaan perpustakaan_id_admin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perpustakaan
    ADD CONSTRAINT perpustakaan_id_admin_foreign FOREIGN KEY (id_admin) REFERENCES public.users(id);


--
-- Name: setting_admin setting_admin_id_admin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.setting_admin
    ADD CONSTRAINT setting_admin_id_admin_foreign FOREIGN KEY (id_admin) REFERENCES public.users(id);


--
-- Name: siswa siswa_id_sekolah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siswa
    ADD CONSTRAINT siswa_id_sekolah_foreign FOREIGN KEY (id_sekolah) REFERENCES public.sekolah(id);


--
-- Name: siswa_kelas siswa_kelas_id_kelas_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siswa_kelas
    ADD CONSTRAINT siswa_kelas_id_kelas_foreign FOREIGN KEY (id_kelas) REFERENCES public.kelas(id);


--
-- Name: siswa_kelas siswa_kelas_id_siswa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siswa_kelas
    ADD CONSTRAINT siswa_kelas_id_siswa_foreign FOREIGN KEY (id_siswa) REFERENCES public.siswa(id);


--
-- PostgreSQL database dump complete
--

