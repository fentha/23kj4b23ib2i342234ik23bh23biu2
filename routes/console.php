<?php

use Illuminate\Foundation\Inspiring;
use Symfony\Component\Process\Process;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('devover:auto-build', function () {
    $this->comment('Memulai penghapusan tabel..');
    $this->comment(shell_exec('php artisan db:wipe'));
    $this->comment('Penghapusan tabel sukses !');
    $this->comment('-------------------------------------');
    $this->comment('Memulai migrasi tabel..');
    $this->comment(shell_exec('php artisan migrate'));
    $this->comment('Migrasi tabel sukses..');
    $this->comment('-------------------------------------');
    $this->comment('Memulai inject database..');
    $this->comment(shell_exec('php artisan db:seed'));
    $this->comment('Inject database sukses..');
    $this->comment('-------------------------------------');
})->describe('Reset tables database absensi1');