<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=>'auth','prefix'=>'ajax'], function(){
	Route::post('/idgenerator', 'AjaxController@generatorId');
});	

Route::group(['middleware'=>'auth','prefix'=>'page'], function(){

	Route::group(['prefix'=>'home'], function(){

		Route::get('/', function () {
		    return view('page.welcome.index');
		});
		

    });

	Route::group(['prefix'=>'siswa'], function(){

		Route::get('/', 'SiswaController@ShowSiswa');
		
		Route::get('/is_admin/{id}', 'SiswaController@ShowSiswaIsAdmin');
		
		Route::get('/gls/{nis}', 'SiswaController@ShowGlsSiswa');

		Route::post('/add', 'SiswaController@AddSiswa');

		Route::get('/delete/{id}', 'SiswaController@DeleteSiswa');

		Route::get('/edit', function () {
		    return view('page.siswa.edit');
		});

		Route::get('/edit/{id}', 'SiswaController@EditSiswa');

		Route::post('/edit/save', 'SiswaController@EditSiswaSave');		

		Route::get('/print-qrcode/{id}',  'SiswaController@PrintQrcode');
		
		Route::get('/print-qrcode-all',  'SiswaController@PrintQrcodeAll');

		Route::get('/pilih-design',  'SiswaController@PilihDesign');


    });

    Route::group(['prefix'=>'buku'], function(){


		Route::post('/save', 'PerpustakaanController@AddPerpustakaan');

		Route::get('/', 'PerpustakaanController@ShowPerpustakaan');

		Route::get('/tambah-buku', function () {
		    return view('page.perpustakaan.tambah_buku');
		});

		Route::get('/edit/{id}', 'PerpustakaanController@EditPerpustakaan');

		Route::post('/edit/save', 'PerpustakaanController@EditPerpustakaanSave');

		Route::get('/delete/{id}', 'PerpustakaanController@DeletePerpustakaan');

		Route::get('/print-barcode/{id}',  'PerpustakaanController@PrintBarcode');

		Route::get('/peminjaman',  'PerpustakaanController@showpagepeminjaman');
				
    });


    Route::group(['prefix'=>'absensi'], function(){

		Route::get('/hari-ini', 'AbsensiController@ShowAbsensi');

		Route::get('/hari-ini/{id}', 'AbsensiController@ShowFindAbsensi');

		Route::post('/hari-ini','AbsensiController@AbsensiSave');


		//rekap

		Route::get('/rekap-absensi/{table}','AbsensiController@ShowKelas');

		Route::get('/rekap-absensi/kelas/{id}','AbsensiController@DetailKelas');


		//Route::get('/rekap-absensi', 'AbsensiController@index');
		Route::post('/rekap-absensi/fetch_data', 'AbsensiController@fetch_data')->name('daterange.fetch_data');
    });



    Route::group(['prefix'=>'gls'], function(){

		Route::get('/melihat', 'GlsController@ShowGlsMelihat');

		Route::get('/melihat/delete/{id}', 'GlsController@DeleteMelihat');

		Route::get('/membaca', 'GlsController@ShowGlsMembaca');

		Route::get('/membaca/delete/{id}', 'GlsController@DeleteMembaca');

		Route::get('/mendengar', 'GlsController@ShowGlsMendengar');

		Route::get('/mendengar/delete/{id}', 'GlsController@DeleteMendengar');

    });

    Route::group(['prefix'=>'karakter'], function(){

    	Route::post('/opsi-pelanggaran/add', 'KarakterController@AddPelanggaran');

		Route::get('/opsi-pelanggaran', 'KarakterController@ShowOpsiPelanggaran');

		Route::get('/opsi-pelanggaran/delete/{id}', 'KarakterController@DeletePelanggaran');

		Route::get('/tema-penilaian-karakter', 'KarakterController@ShowTemaKarakter');
		
		Route::post('/tema-penilaian-karakter/save', 'KarakterController@AddTemaKarakter');
		
		Route::get('/tema-penilaian-karakter/delete/{id}', 'KarakterController@DeleteTemaKarakter');

		Route::get('/opsi-penilaian-karakter', 'KarakterController@ShowOpsiPenilaianKarakter');

		Route::post('/opsi-penilaian-karakter/save', 'KarakterController@AddOpsiKarakter');

		Route::get('/penilaian-karakter', 'KarakterController@ShowPenilaianKarakter');

		Route::get('/pelanggaran-siswa', 'KarakterController@ShowPelanggaranSiswa');

		Route::get('/pelanggaran-siswa/get-siswa', 'KarakterController@getSiswaList');

		Route::post('/pelanggaran-siswa/save', 'KarakterController@AddPelanggaranSiswa');

		Route::get('/pelanggaran-siswa/delete/{id}', 'KarakterController@DeletePelanggaranSiswa');

		Route::get('/point-pelanggaran', function () {
		    return view('page.karakter.point_pelanggaran');
		});

    });

    Route::group(['prefix'=>'setting'], function(){
    	
		Route::get('/', 'SettingController@ShowSetting');
		Route::post('/', 'SettingController@EditSetting');

    });

    Route::group(['prefix'=>'sekolah'], function(){


    	Route::get('/', 'SekolahController@ShowSekolah');

    	Route::post('/save', 'SekolahController@AddSekolah');

		Route::get('/delete/{id}', 'SekolahController@DeleteSekolah');

		Route::get('/edit/{id}', 'SekolahController@EditSekolah');

		Route::post('/edit/save', 'SekolahController@EditSekolahSave');

		//siswakelas

		Route::get('/siswakelas/{id}', 'SiswaKelasController@add');

		Route::post('/siswakelas/', 'SiswaKelasController@create');


		//kelas

		Route::get('/kelas', 'SekolahController@ShowKelas');

    	Route::get('/kelas/{id}', 'SekolahController@Show');

		Route::post('/kelas/save', 'SekolahController@AddKelas');

		Route::get('/kelas/delete/{id}', 'SekolahController@DeleteKelas');

		Route::get('/kelas/detail-siswa/{id}', 'SekolahController@ShowDetailSiswaKelas');
		
		Route::get('/kelas/detail-siswa/{id}/tambah-siswa', 'SekolahController@ShowTambahSiswaKelas');
		
		Route::get('/kelas/detail-siswa/{id}/tambah-kenaikan-kelas', 'SekolahController@ShowTambahKenaikanSiswaKelas');

		//guru

		Route::get('/guru', 'SekolahController@ShowGuru');

    	Route::post('/guru/save', 'SekolahController@AddGuru');

    	Route::get('/guru/edit/{id}', 'SekolahController@EditGuru');

		Route::post('/guru/edit/save', 'SekolahController@EditGuruSave');


		Route::get('/guru/delete/{id}', 'SekolahController@DeleteGuru');

		//admin sekolah

		Route::get('/admin', 'SekolahController@ShowAdminSekolah');

    	Route::post('/admin/save', 'SekolahController@AddAdminSekolah');

    	Route::get('/admin/edit/{id}', 'SekolahController@EditAdminSekolah');

		Route::post('/admin/edit/save', 'SekolahController@EditAdminSekolahSave');

		Route::get('/admin/delete/{id}', 'SekolahController@DeleteAdminSekolah');

		//tahun ajarn

		Route::get('/tahun-ajaran', 'SekolahController@ShowTahunAjaran');

    	Route::post('/tahun-ajaran/save', 'SekolahController@AddTahunAjaran');

    	Route::get('/tahun-ajaran/edit/{id}', 'SekolahController@EditTahunAjaran');

		Route::post('/tahun-ajaran/edit/save', 'SekolahController@EditTahunAjaranSave');

		Route::get('/tahun-ajaran/delete/{id}', 'SekolahController@DeleteTahunAjaran');

		//data anjungan
		Route::get('/anjungan', 'SekolahController@ShowDataAnjungan');

    	Route::post('/anjungan/save', 'SekolahController@AddAnjunganSekolah');
    });

    Route::group(['prefix'=>'user'], function(){

    	Route::get('/', 'UsersController@ShowUsers');

    	Route::post('/save', 'UsersController@AddUsers');

		Route::get('/delete/{id}', 'UsersController@DeleteUsers');

		Route::get('/edit/{id}', 'UsersController@EditUsers');

		Route::post('/edit/save', 'UsersController@EditUsersSave');
		

    });

    Route::group(['prefix'=>'kartu', 'middleware'=>'administrator'], function(){
    	
    	Route::group(['prefix'=>'design'], function(){

	    	Route::get('/', 'KartuController@designPage');
	    	
	    	Route::get('/{kid}/layout/{lid}/front/style', 'KartuController@designLayoutFrontDashboard');

	    	Route::post('/front/save', 'KartuController@designLayoutFrontDashboardSave');

	    	Route::post('/layout/front/bg', 'KartuController@designBgFrontDashboardSave');

	    	Route::get('/{id}/layout/back/style', 'KartuController@designLayoutBackDashboard');
	    	
	    	Route::post('/{id}/layout/back/style', 'KartuController@designLayoutBackDashboardSave');

	    	Route::post('/layout/back/bg', 'KartuController@designBgBackDashboardSave');
    		
    	});
    	
    	Route::post('/design/save', 'KartuController@designSave');

    	Route::get('/layout', 'KartuController@layoutPage');

    	// Route::get('/layout/{designid}/setting/{layoutid}', 'KartuController@layoutSettingPage');
    	
    	Route::get('/layout/save', 'KartuController@layoutSave');

    	Route::get('/cetak', 'KartuController@cetakPage');
    	
    	Route::get('/cetak/{type}/all/{idsekolah}', 'KartuController@cetakKartu');

    	Route::get('/cetak/all', 'KartuController@cetakAll');

    	Route::get('/cetak/siswa/{id}', 'KartuController@cetakSingle');


    });

});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

# update to server
