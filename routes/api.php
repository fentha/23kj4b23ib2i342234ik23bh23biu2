<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'public'], function(){

	/* Absensi */
	Route::group(['prefix'=>'absensi'], function(){
		Route::post('/proses', 'AbsensiController@prosesAbsensi');
	});

	/* GLS */
	Route::group(['prefix'=>'gls'], function(){
		Route::post('/buku/detail', 'PerpustakaanController@detailBuku');
		Route::post('/membaca/save', 'GlsController@saveGlsMembaca');
		Route::post('/mendengar/save', 'GlsController@saveGlsMendengar');
		Route::post('/melihat/save', 'GlsController@saveGlsMelihat');
	});

	/* Penilaian Karakter */
	Route::group(['prefix'=>'penilaian-karakter'], function(){
		Route::post('/tema/list', 'KarakterController@listTemaPenilaianKarakter');
		Route::post('/tema/list/detail', 'KarakterController@listDetailTemaPenilaianKarakter');
		Route::post('/penilaian/save', 'KarakterController@savePenilaianKarakter');
		Route::post('/category/list/option', 'KarakterController@listPenilaianKarakter');
	});

});

