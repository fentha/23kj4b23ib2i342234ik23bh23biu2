<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SiswaKelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'siswa_kelas';

    public function up()
    {
         Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_siswa')->unsigned();
                $t->foreign('id_siswa')->references('id')->on('siswa');
            $t->integer('id_kelas')->unsigned();
                $t->foreign('id_kelas')->references('id')->on('kelas');
            $t->integer('id_tahun_ajaran')->unsigned();
                $t->foreign('id_tahun_ajaran')->references('id')->on('tahun_ajaran');          
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}
