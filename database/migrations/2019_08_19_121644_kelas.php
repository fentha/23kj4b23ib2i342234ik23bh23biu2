<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'kelas';


    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->string('nama_kelas');
            $t->integer('id_user')->unsigned()->nullable();
                $t->foreign('id_user')->references('id')->on('users');
            $t->integer('id_sekolah')->unsigned();
                $t->foreign('id_sekolah')->references('id')->on('sekolah');
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}
