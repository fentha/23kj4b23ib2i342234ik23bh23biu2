<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KartuSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = 'kartu_setting';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id'); 
            $t->integer('id_design')->unsigned()->nullable();
                $t->foreign('id_design')->references('id')->on('design_kartu');
            $t->string('setting_title');  
            $t->text('setting_value')->nullable();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);  
    }
}
