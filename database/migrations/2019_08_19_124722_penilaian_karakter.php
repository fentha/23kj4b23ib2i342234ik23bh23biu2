<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PenilaianKarakter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'penilaian_karakter';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_siswa')->unsigned();
                $t->foreign('id_siswa')->references('id')->on('siswa'); 
            $t->integer('id_opsi_penilaian_karakter')->unsigned();
                $t->foreign('id_opsi_penilaian_karakter')->references('id')->on('opsi_penilaian_karakter');                        
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}
