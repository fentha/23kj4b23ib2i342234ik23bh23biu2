<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Gls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'gls';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->enum('gls_kategori',['membaca','mendengar','melihat']);
            $t->integer('id_buku')->unsigned()->nullable();
                $t->foreign('id_buku')->references('id')->on('perpustakaan');
            $t->integer('id_siswa')->unsigned();
                $t->foreign('id_siswa')->references('id')->on('siswa');                 
            $t->string('gls_halaman')->nullable();                      
            $t->string('gls_rangkuman');                   
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}
