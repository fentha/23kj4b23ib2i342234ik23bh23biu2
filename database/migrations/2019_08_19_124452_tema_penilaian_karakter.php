<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TemaPenilaianKarakter extends Migration
{
    protected $table = 'tema_penilaian_karakter';


    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_sekolah')->unsigned()->nullable();
                $t->foreign('id_sekolah')->references('id')->on('sekolah');
            $t->string('tema_penilaian');            
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}
