<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Siswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'siswa';



    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id'); 
            $t->integer('id_sekolah')->unsigned()->nullable();
                $t->foreign('id_sekolah')->references('id')->on('sekolah');
            $t->string('siswa_nis');
            $t->string('siswa_nama')->nullable();
            $t->string('siswa_alamat')->nullable();
            $t->string('siswa_foto')->nullable();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists($this->table);
    }
}
