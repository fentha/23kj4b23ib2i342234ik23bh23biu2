<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Absensi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'absensi';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_siswa')->unsigned();
                $t->foreign('id_siswa')->references('id')->on('siswa');
            $t->datetime('absensi_waktu');            
            $t->enum('absensi_status',['alfa','izin','sakit','hadir','pulang','terlambat'])->default('hadir');
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
