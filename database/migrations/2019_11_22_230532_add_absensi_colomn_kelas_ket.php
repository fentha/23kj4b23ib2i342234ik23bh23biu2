<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAbsensiColomnKelasKet extends Migration
{

    protected $table = 'absensi';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function($t) {
        //     $t->integer('id_kelas')->unsigned()->default(0);
        //         $t->foreign('id_kelas')->references('id')->on('kelas');
            $t->string('absensi_keterangan')->default("-");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
