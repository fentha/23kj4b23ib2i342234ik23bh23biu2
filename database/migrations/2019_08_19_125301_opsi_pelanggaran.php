<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OpsiPelanggaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'opsi_pelanggaran';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_admin')->unsigned();
                $t->foreign('id_admin')->references('id')->on('users');
            $t->string('nama_pelanggaran');                     
            $t->string('bobot_pelanggaran');                     
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}
