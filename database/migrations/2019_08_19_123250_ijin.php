<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ijin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'ijin';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_siswa')->unsigned();
                $t->foreign('id_siswa')->references('id')->on('siswa');
            $t->datetime('ijin_tanggal_mulai');
            $t->datetime('ijin_tanggal_selesai');
            $t->enum('ijin_status',['ijin','sakit'])->default('ijin');
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}
