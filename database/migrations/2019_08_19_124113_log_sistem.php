<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LogSistem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'log_sistem';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->string('log_title')->nullable();
            $t->string('log_value')->nullable();            
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}
