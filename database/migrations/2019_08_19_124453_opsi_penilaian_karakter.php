<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OpsiPenilaianKarakter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'opsi_penilaian_karakter';


    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_sekolah')->unsigned()->nullable();
                $t->foreign('id_sekolah')->references('id')->on('sekolah');
            $t->integer('id_tema_penilaian_karakter');
                $t->foreign('id_tema_penilaian_karakter')->references('id')->on('tema_penilaian_karakter');          
            $t->string('opsi_konten');            
            $t->string('opsi_bobot_nilai');            
            $t->integer('id_admin')->unsigned();
                $t->foreign('id_admin')->references('id')->on('users');          
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}
