<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Perpustakaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'perpustakaan';


    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_admin')->unsigned();
                $t->foreign('id_admin')->references('id')->on('users');
            $t->string('perpustakaan_barcode');
            $t->string('perpustakaan_isbn');
            $t->string('perpustakaan_judul_buku');
            $t->string('perpustakaan_pengarang')->nullable();
            $t->string('perpustakaan_penerbit')->nullable();
            $t->string('perpustakaan_kode_buku')->nullable();          
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}