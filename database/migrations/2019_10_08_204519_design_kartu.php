<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DesignKartu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'design_kartu';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id'); 
            $t->string('design')->nullable();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists($this->table);        
    }
}
