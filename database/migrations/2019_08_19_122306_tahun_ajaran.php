<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TahunAjaran extends Migration
{
    protected $table = 'tahun_ajaran';

    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_admin')->unsigned();
                $t->foreign('id_admin')->references('id')->on('users');
            $t->string('ta_semester');  
            $t->string('ta_tahun_ajaran');  
            $t->enum('ta_status',[0,1])->default(1);  
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
