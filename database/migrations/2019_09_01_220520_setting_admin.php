<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SettingAdmin extends Migration
{
    protected $table = 'setting_admin';

    /**
     * Run the migrations.
     *
     * nomor register tilang, nama pelanggar, pasal, tanggal sidang, barang bukti, tanggal dibuat
     * @return void
     */

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_admin')->unsigned();
                $t->foreign('id_admin')->references('id')->on('users');
            $t->string('setting_title');  
            $t->text('setting_value')->nullable();  
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
