<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PelanggaranSiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pelanggaran_siswa';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $t){
            $t->increments('id');
            $t->integer('id_siswa')->unsigned();
                $t->foreign('id_siswa')->references('id')->on('siswa'); 
            $t->integer('id_ta')->unsigned();
                $t->foreign('id_ta')->references('id')->on('tahun_ajaran'); 
            $t->integer('id_opsi_pelanggaran')->unsigned();
                $t->foreign('id_opsi_pelanggaran')->references('id')->on('opsi_pelanggaran');
            $t->integer('id_admin')->unsigned();
                $t->foreign('id_admin')->references('id')->on('users');           
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);        
    }
}
