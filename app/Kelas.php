<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Kelas extends Model
{
    protected $table = 'kelas';

    protected $fillable = [
		'id',
		'nama_kelas',
	];

	protected $hidden = [
	];

	public function Siswa()
    {
        return $this->belongsToMany('App\Siswa', 'siswa_kelas', 'id_kelas', 'id_siswa')->withPivot('id');
    }
    public function absen()
    {    
        return $this->belongsToMany('App\Siswa', 'absensi', 'id_siswa', 'id_kelas')->withPivot('absensi_status', 'absensi_waktu', 'absensi_keterangan')->wherePivot('absensi_waktu', Carbon::now('Asia/Jakarta')->format('Y-m-d'));
    }

}
