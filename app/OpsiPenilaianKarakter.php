<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpsiPenilaianKarakter extends Model
{
    protected $table = 'opsi_penilaian_karakter';

    protected $fillable = [
		'id_sekolah',		
		'id_tema_penilaian_karakter',		
		'opsi_konten',
		'opsi_bobot_nilai',
		'id_admin',
	];

	protected $hidden = [
	];
}
