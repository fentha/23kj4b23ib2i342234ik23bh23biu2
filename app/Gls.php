<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gls extends Model
{
    protected $table = 'gls';

    protected $fillable = [
		'id',
		'gls_kategori',		
		'id_buku',
		'id_siswa',
		'gls_judul',
		'gls_halaman',
		'gls_rangkuman',
	];

	protected $hidden = [
	];
}
