<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian_Karakter extends Model
{
    protected $table = 'penilaian_karakter';

    protected $fillable = [
		'id',
		'id_siswa',		
		'id_opsi_penilaian_karakter',
		'id_admin',
	];

	protected $hidden = [
	];
}
