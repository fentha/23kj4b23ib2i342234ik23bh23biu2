<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Siswa extends Model
{
    protected $table = 'siswa';

    protected $fillable = [
		'id',
		'siswa_nis',		
		'siswa_nama',
		'siswa_alamat',
		'siswa_foto',
	];

	protected $hidden = [
	];
}
