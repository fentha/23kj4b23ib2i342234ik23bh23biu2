<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingAdmin extends Model
{
    protected $table = "setting_admin";
}
