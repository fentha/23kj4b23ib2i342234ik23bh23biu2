<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignKartu extends Model
{
    protected $table = 'design_kartu';

    protected $fillable = [
		'id',
		'design',	
	];

	protected $hidden = [
	];
}
