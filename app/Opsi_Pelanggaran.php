<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opsi_Pelanggaran extends Model
{
    protected $table = 'opsi_pelanggaran';

    protected $fillable = [
		'id',
		'id_admin',		
		'nama_pelanggaran',
		'bobot_pelanggaran',
	];

	protected $hidden = [
	];
}
