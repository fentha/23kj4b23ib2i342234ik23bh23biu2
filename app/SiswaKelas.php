<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiswaKelas extends Model
{
    protected $table = 'siswa_kelas';

    protected $fillable = [
		'id',
		'id_siswa',
		'id_kelas',
		'id_tahun_ajaran',
	];

	protected $hidden = [
	];
}
