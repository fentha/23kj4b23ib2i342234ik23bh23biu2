<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perpustakaan extends Model
{
    protected $table = 'perpustakaan';

    protected $fillable = [
		'id',
		'id_admin',		
		'perpustakaan_isbn',
		'perpustakaan_judul_buku',
		'perpustakaan_pengarang',
		'perpustakaan_penerbit',
		'perpustakaan_kode_buku',
		'perpustakaan_kbarcode',
	];

	protected $hidden = [
	];
}
