<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KartuSetting extends Model
{
    protected $table = 'kartu_setting';
}
