<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemaPenilaianKarakter extends Model
{
    protected $table = 'tema_penilaian_karakter';
}
