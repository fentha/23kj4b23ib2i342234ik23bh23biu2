<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perpustakaan;
use App\Siswa;
use App\Http\Controllers\PandoraController as Pandora;

use PDF;

class PerpustakaanController extends Controller
{
    public function AddPerpustakaan(Request $r)
    {  

        $data                           = new Perpustakaan;
        $data->id_admin                 = 1;
        $data->perpustakaan_barcode       = $r->perpus_barcode;
        $data->perpustakaan_isbn           = $r->perpus_isbn;
        $data->perpustakaan_judul_buku     = $r->perpus_judul;
        $data->perpustakaan_pengarang   = $r->perpus_pengarang;
        $data->perpustakaan_penerbit    = $r->perpus_penerbit;
        $data->perpustakaan_kode_buku   = $r->perpus_kode;
        $data->save();

        return redirect('/page/buku');

    }

    public function ShowPerpustakaan()
    {
        $data = Perpustakaan::all();
        
        return view('page.perpustakaan.buku')->with('data', $data);
    }

    public function EditPerpustakaan(Request $r)
    {
        $data = Perpustakaan::find($r->id);
    
        return view('page.perpustakaan.edit')->with('data', $data);
    }

    public function EditPerpustakaanSave(Request $r)
    {  

        $data                           = Perpustakaan::find($r->idbuku);
        $data->perpustakaan_barcode       = $r->perpus_barcode;
        $data->perpustakaan_isbn           = $r->perpus_isbn;
        $data->perpustakaan_judul_buku     = $r->perpus_judul;
        $data->perpustakaan_pengarang   = $r->perpus_pengarang;
        $data->perpustakaan_penerbit    = $r->perpus_penerbit;
        $data->perpustakaan_kode_buku   = $r->perpus_kode;
        $data->save();

        return redirect('/page/buku');

    }

    public function DeletePerpustakaan($id)
    {

        $data = Perpustakaan::findOrFail($id);
        $data->delete();

        return redirect('/page/buku');
    }

    public  function PrintBarcode(Request $r)
    { 
        $barcode =  Perpustakaan::limit(12)->find($r->id);
        $pdf =  PDF::loadView('page.perpustakaan.cetak_barcode', ['barcode'=>$barcode]);
        return $pdf->stream(); 

    }

    public function showpagepeminjaman(){
        return view("page.perpustakaan.peminjaman");
    }

    /*======================================
    =            API Controller            =
    ======================================*/
    
    public function detailBuku(Request $r)
    {
        $getData        = Perpustakaan::select(['perpustakaan_judul_buku','perpustakaan_pengarang','perpustakaan_penerbit'])->where('perpustakaan_barcode', $r->barcodeBuku)->get()->first();

        $getData['user'] = strtoupper(Siswa::where('siswa_nis', $r->nis)->get()->first()->siswa_nama);

        if($getData) {
            $msg         = null;
            return Pandora::RespondsData(200, $getData, null, $msg);
        } else {
            return Pandora::RespondsData(500, null, null, 'Data buku tidak ditemukan');
        }
    }
    
    /*=====  End of API Controller  ======*/
    

}
