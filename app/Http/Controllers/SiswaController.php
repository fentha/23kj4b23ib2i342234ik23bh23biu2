<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\Sekolah;
use App\Gls;
use App\DesignKartu;
use App\SiswaKelas;
use App\TahunAjaran;
use Auth;
use view;
use PDF;

class SiswaController extends Controller
{
    public function AddSiswa(Request $r)
    {
        try {
            if($r->file('s_foto') != null) {
                // Image settings
                $fimgPath = "images/siswa_photo/";
                $fimgname = "photo_".$r->siswa_nis."_".time();
                $fimg     = $r->file('s_foto');
                $fimgext  = $fimg->getClientOriginalExtension();
                $fullPath = $fimgPath.$fimgname.".".$fimgext;

                // Image compress and save
                // \Image::make($fimg->save($fullPath)->resizeImage(1024, null));
                \Image::make($fimg)->resize(
                    1024, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(-90)->save($fullPath);
                \Image::make($fimg)->resize(
                    250, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(-90)->save("thumb_image/".$fullPath);

            } else {
                $fullPath = null;
            }
       
            $data               = new Siswa;
            if($r->id_sekolah !== ''){
                $data->id_sekolah   = $r->id_sekolah;            
            } else {
                $data->id_sekolah   = Auth::user()->id_sekolah;            
            }
            $data->siswa_nis    = $r->s_nis;
            $data->siswa_nama   = $r->s_nama;
            $data->siswa_alamat = $r->s_alamat;
            $data->siswa_foto   = $fullPath;
            $data->save();

            if($r->s_kelas !== ''){
                $kelas              = new SiswaKelas;
                $kelas->id_siswa    = $data->id;
                $kelas->id_kelas    = $r->s_kelas;
                $kelas->id_tahun_ajaran = TahunAjaran::where('ta_status',1)->get()->first()->id;
                $kelas->save();
            }

            $msg['status']      = 200;
            $msg['title']       = "Data Sukses Tersimpan";
            $msg['text']        = "Data siswa telah disimpan";
            $msg['footer']      = null;

            return redirect('/page/siswa')->with('message', $msg);

        } catch (Exception $e){
            $msg['status']      = 500;
            $msg['title']       = "Data Gagal Tersimpan";
            $msg['text']        = "Periksa kembali kesalahan input yang anda lakukan";
            $msg['footer']      = "Apabila kesulitan silahkan hubungi operator";
            
            return redirect('/page/siswa')->with('message', $msg);
        }        
    }

    public function ShowSiswa()
    {
        $data = Siswa::where('id_sekolah',Auth::user()->id_sekolah)->get();
        return view('page.siswa.main')->with('data', $data);
    }

    public function ShowSiswaIsAdmin($id)
    {
        $data        = Siswa::where('id_sekolah',$id)->get();
        $datasekolah = Sekolah::find($id); 
        return view('page.siswa.main')
            ->with('data_sekolah', $datasekolah)
            ->with('data', $data);
    }


    public function DeleteSiswa($id)
    {

        $data = Siswa::findOrFail($id);
        $data->delete();

        if($data){
            $msg['status']      = 200;
            $msg['title']       = "Data Sukses Dihapus";
            $msg['text']        = "Data siswa telah sukses dihapuskan";
            $msg['footer']      = null;
        } else {
            $msg['status']      = 500;
            $msg['title']       = "Data Gagal Dihapus";
            $msg['text']        = "Terjadi kesalahan sistem";
            $msg['footer']      = "Apabila kesulitan silahkan hubungi operator";
        }

        return redirect('/page/siswa')->with('message', $msg);
    }

    public function EditSiswa(Request $r)
    {
        $data = Siswa::find($r->id);
        return view('page.siswa.edit')->with('data', $data);
    }

    public function EditSiswaSave(Request $r)
    {
        try {
            if($r->file('s_foto') != null) {
                // Image settings
                $fimgPath = "images/siswa_photo/";
                $fimgname = "photo_".$r->siswa_nis."_".time();
                $fimg     = $r->file('s_foto');
                $fimgext  = $fimg->getClientOriginalExtension();
                $fullPath = $fimgPath.$fimgname.".".$fimgext;

                // Image compress and save
                // \Image::make($fimg->save($fullPath)->resizeImage(1024, null));
                \Image::make($fimg)->resize(
                    1024, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(-90)->save($fullPath);
                \Image::make($fimg)->resize(
                    250, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(-90)->save("thumb_image/".$fullPath);

                $data               = siswa::find($r->idsiswa);
                $data->siswa_nis    = $r->s_nis;
                $data->siswa_nama   = $r->s_nama;
                $data->siswa_alamat = $r->s_alamat;
                $data->siswa_foto   = $fullPath;
                $data->save();

            } else {
                $data               = siswa::find($r->idsiswa);
                $data->siswa_nis    = $r->s_nis;
                $data->siswa_nama   = $r->s_nama;
                $data->siswa_alamat = $r->s_alamat;
                $data->save();
            }

            $msg['status']      = 200;
            $msg['title']       = "Data Sukses Tersimpan";
            $msg['text']        = "Data siswa telah disimpan";
            $msg['footer']      = null;

            return redirect('/page/siswa')->with('message', $msg);

        } catch (Exception $e){
            $msg['status']      = 500;
            $msg['title']       = "Data Gagal Tersimpan";
            $msg['text']        = "Periksa kembali kesalahan input yang anda lakukan";
            $msg['footer']      = "Apabila kesulitan silahkan hubungi operator";
            
            return redirect('/page/siswa')->with('message', $msg);
        }        
    }

    public function ShowGlsSiswa(Request $r)
    {
        $id_siswa       = Siswa::where('siswa_nis', $r->nis)->get()->first()->id;
        $data_membaca   = Gls::where('id_siswa', $id_siswa)
            ->leftJoin('siswa', 'siswa.id', 'gls.id_siswa')
            ->leftJoin('perpustakaan', 'perpustakaan.id', 'gls.id_buku')
            ->select(
                [
                    'gls.*',
                    'gls.created_at as cat',
                    'siswa.*',
                    'perpustakaan.*',
                ]
            )
            ->where('gls_kategori', 'membaca')
            ->orderBy('gls.created_at', 'desc')
            ->get();

        $data_melihat   = Gls::where('id_siswa', $id_siswa)
            ->leftJoin('siswa', 'siswa.id', 'gls.id_siswa')
            ->select(
                [
                    'gls.*',
                    'gls.created_at as cat',
                    'siswa.*',
                ]
            )
            ->where('gls_kategori', 'melihat')
            ->orderBy('gls.created_at', 'desc')
            ->get();

        $data_mendengar   = Gls::where('id_siswa', $id_siswa)
            ->leftJoin('siswa', 'siswa.id', 'gls.id_siswa')
            ->select(
                [
                    'gls.*',
                    'gls.created_at as cat',
                    'siswa.*',
                ]
            )
            ->where('gls_kategori', 'mendengar')
            ->orderBy('gls.created_at', 'desc')
            ->get();

        return view('page.siswa.detail_gls')
            ->with('data_membaca', $data_membaca)
            ->with('data_melihat', $data_melihat)
            ->with('data_mendengar', $data_mendengar);
    }

    public  function PrintQrcode(Request $r)
    { 
        $qrcode =  Siswa::limit(12)->find($r->id);
        $pdf =  PDF::loadView('page.siswa.cetak_qrcode', ['qrcode'=>$qrcode]);
        return $pdf->stream(); 
    }

    public  function PrintQrcodeAll(Request $r)
    { 
        // $qrcode =  Siswa::limit(8)->get();
        $qrcode =  Siswa::limit(1)->get();
        // $qrcode =  Siswa::all();
        return view('page.siswa.cetak_qrcode_all')->with('qrcode', $qrcode);

        // $pdf =  PDF::loadView('page.siswa.cetak_qrcode_all',['qrcode'=>$qrcode]);
        // return $pdf->stream(); 

        
        // return PDF::loadView('page.siswa.cetak_qrcode_all',['qrcode'=>$qrcode])->setPaper('a4', 'potrait')->setWarnings(false)->stream();

    }

    public function PilihDesign()
    {
        $data = DesignKartu::all();
        return view('page.siswa.pilih_design')->with('data', $data);
    }


    /*===================================
    =            API Service            =
    ===================================*/

    public function ApiSaveSiswa(Request $r)
    {

        try {

            if($r->file('photo') != null) {
                // Image settings
                $fimgPath = "images/siswa_photo/";
                $fimgname = "photo_".$r->siswa_nis."_".time();
                $fimg     = $r->file('photo');
                $fimgext  = $fimg->getClientOriginalExtension();
                $fullPath = $fimgPath.$fimgname.".".$fimgext;

                // Image compress and save
                // \Image::make($fimg->save($fullPath)->resizeImage(1024, null));
                \Image::make($fimg)->resize(
                    1024, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(-90)->save($fullPath);
                \Image::make($fimg)->resize(
                    250, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(-90)->save("thumb_image/".$fullPath);

            } else {
                $fullPath = null;
            }

            if(Siswa::where('siswa_nis', $r->siswa_nis)->get()->first() == null) {
                $data = new Siswa();
                $data->siswa_nis           = $r->siswa_nis;
                $data->siswa_foto       = $fullPath;
                $data->save();

                $data['status'] = "200";
                return json_encode($data);
            } else {
                $data['status'] = "";
                return json_encode($data);
            }

        } catch (Exception $e){
            $data['status'] = 'error';
            return json_encode($data);
        }

    }

}
