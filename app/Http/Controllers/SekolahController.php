<?php

namespace App\Http\Controllers;
use App\Sekolah;
use App\Kelas;
use App\Siswa;
use App\User;
use App\TahunAjaran;
use auth;


use Illuminate\Http\Request;

class SekolahController extends Controller
{
    public function ShowSekolah()
    {
        $data = Sekolah::all();
        
        return view('page.sekolah.main')->with('data', $data);
    }

    public function AddSekolah(Request $r)
    {

        $data                   = new Sekolah;
        $data->nama_sekolah     = $r->nama_sekolah;
        $data->save();

        return redirect('/page/sekolah');


    }

    public function DeleteSekolah($id)
    {

        $data = Sekolah::findOrFail($id);


        $data -> delete();

        return redirect('/page/sekolah');
    }

    public function EditSekolah(Request $r)
    {
        $data = Sekolah::find($r->id);

    
        return view('page.sekolah.edit')->with('data', $data);
    }

    public function EditSekolahSave(Request $r)
    {


        $data                   = Sekolah::find($r->idsekolah);
        $data->nama_sekolah     = $r->nama_sekolah;
        $data->save();

        return redirect('/page/sekolah');


    }

    public function ShowKelas()
    {
        //$data = Kelas::all();

        $data = Kelas::select(
            [
                    'kelas.*',
                    'users.*',
                    'kelas.id as kelid',
            ]
        )
            ->leftJoin('users', 'users.id', 'kelas.id_user')
            ->orderBy('name', 'asc')
            ->get();
        
        return view('page.sekolah.kelas')->with('data', $data);
    }

    public function AddKelas(Request $r)
    {

        $data                 = new Kelas;
        $data->nama_kelas     = $r->kel_namakelas;
        $data->id_user        = $r->kel_walikelas;        
        $data->id_sekolah     = Auth::user()->id_sekolah;
        $data->save();

        return redirect('/page/sekolah/kelas');

    }

    public function DeleteKelas($id)
    {

        $data = Kelas::findOrFail($id);


        $data -> delete();

        return redirect('/page/sekolah/kelas');
    }

    public function show($id){
        $data = Kelas::find($id);
        return view('page/sekolah/detail_siswa2', ['data'=>$data]);
    }

    public function ShowDetailSiswaKelas(Request $r)
    {
        $data = Siswa::where('id', '0')->get();
        
        return view('page.sekolah.detail_siswa')
            ->with('kelas', $r->id)
            ->with('data', $data);
    }

    public function ShowTambahSiswaKelas(Request $r)
    {
        $data = Siswa::all();
        
        return view('page.sekolah.tambah_data_siswa')->with('data', $data);
    }

    public function ShowTambahKenaikanSiswaKelas(Request $r)
    {
        $data = Siswa::all();
        
        return view('page.sekolah.tambah_data_kenaikan_kelas')->with('data', $data);
    }

    public function ShowGuru(Request $r)
    {
        $data = User::where('type', 'guru')
            ->where('id_sekolah', Auth::user()->id_sekolah)
            ->leftJoin('sekolah', 'sekolah.id', 'users.id_sekolah')
            ->select(
                [
                    'users.*',
                    'users.id as uid',
                    'sekolah.*',
                    'users.status as ust'
                    ]
            )
            ->get();
        
        return view('page.sekolah.guru')->with('data', $data);
    }

    public function AddGuru(Request $r)
    {
        if($r->user_password != $r->user_repassword) {
            return redirect('/page/sekolah/guru')->with('error', 'Password tidak sama, silahkan input ulang password !');
        } else {
            try{
                if($r->file('s_foto') != null) {
                        // Image settings
                    $fimgPath = "images/guru_photo/";
                    $fimgname = "photo_".$r->nip."_".time();
                    $fimg     = $r->file('s_foto');
                    $fimgext  = $fimg->getClientOriginalExtension();
                    $fullPath = $fimgPath.$fimgname.".".$fimgext;

                        // Image compress and save
                        // \Image::make($fimg->save($fullPath)->resizeImage(1024, null));
                    \Image::make($fimg)->resize(
                        1024, null, function ($constraint) {
                            $constraint->aspectRatio(); 
                        }
                    )->rotate(0)->save($fullPath);
                    \Image::make($fimg)->resize(
                        250, null, function ($constraint) {
                            $constraint->aspectRatio(); 
                        }
                    )->rotate(0)->save("thumb_image/".$fullPath);

                } else {
                    $fullPath = null;
                }

                $data               = new User;
                $data->name         = $r->name;
                $data->id_sekolah   = Auth::user()->id_sekolah;
                $data->type         = "guru";
                $data->nip          = $r->nip;
                $data->foto         = $fullPath;
                $data->email        = $r->email;
                $data->password     = bcrypt($r->user_password);
                $data->save();
                return redirect('/page/sekolah/guru');
            } catch(Exeption $e) {
                return redirect('/page/sekolah/guru')->with('error', 'Error : '.$e);
            }
        }
    }

    public function DeleteGuru($id)
    {

        $data = User::findOrFail($id);


        $data -> delete();

        return redirect('/page/sekolah/guru');
    }

    public function EditGuru(Request $r)
    {
        $data = User::find($r->id);

    
        return view('page.sekolah.guru_edit')->with('data', $data);
    }

    public function EditGuruSave(Request $r)
    {
        if($r->user_password != $r->user_repassword) {
            return redirect('/page/sekolah/guru')->with('error', 'Password tidak sama, silahkan input ulang password !');
        } else {
            try{
                if($r->file('user_foto') != null) {
                        // Image settings
                    $fimgPath = "images/guru_photo/";
                    $fimgname = "photo_".$r->nip."_".time();
                    $fimg     = $r->file('user_foto');
                    $fimgext  = $fimg->getClientOriginalExtension();
                    $fullPath = $fimgPath.$fimgname.".".$fimgext;

                    // Image compress and save
                    // \Image::make($fimg->save($fullPath)->resizeImage(1024, null));
                    \Image::make($fimg)->resize(
                        1024, null, function ($constraint) {
                            $constraint->aspectRatio(); 
                        }
                    )->rotate(0)->save($fullPath);
                    \Image::make($fimg)->resize(
                        250, null, function ($constraint) {
                            $constraint->aspectRatio(); 
                        }
                    )->rotate(0)->save("thumb_image/".$fullPath);

                } else {
                    $fullPath = null;
                }

                $data               = User::find($r->idguru);
                $data->name         = $r->user_nama;
                $data->nip          = $r->user_nip;
                $data->id_sekolah   = Auth::user()->id_sekolah;
                $data->type         = "guru";
                if($r->file('user_foto')){
                    $data->foto     = $fullPath;                    
                }
                $data->email        = $r->user_email;
                if($r->user_password){
                    $data->password = bcrypt($r->user_password);
                }
                $data->save();
                return redirect('/page/sekolah/guru');
            } catch(Exeption $e) {
                return redirect('/page/sekolah/guru')->with('error', 'Error : '.$e);
            }
        }

    }

    // Data Anjungan
    public function ShowDataAnjungan()
    {
        $data = User::where('type', 'anjungan_sekolah')
            ->leftJoin('sekolah', 'sekolah.id', 'users.id_sekolah')
            ->select(
                [
                    'users.*',
                    'sekolah.*',
                    'users.status as ust'
                    ]
            )
            ->get();
        
        return view('page.sekolah.anjungan')->with('data', $data);
    }

    public function AddAnjunganSekolah(Request $r)
    {
        if($r->user_password != $r->user_repassword) {
            return redirect('/page/sekolah/anjungan')->with('error', 'Password tidak sama, silahkan input ulang password !');
        } else {
            try{
                $data               = new User;
                $data->name         = $r->user_nama;
                $data->id_sekolah   = $r->user_sekolah;
                $data->type         = "anjungan_sekolah";
                $data->email        = $r->user_email;
                $data->password     = bcrypt($r->user_password);
                $data->save();
                return redirect('/page/sekolah/anjungan');
            } catch(Exeption $e) {
                return redirect('/page/sekolah/anjungan')->with('error', 'Error : '.$e);
            }
        }
    }

    //admin sekolah

    public function ShowAdminSekolah(Request $r)
    {
        $data = User::where('type', 'admin_sekolah')
            ->leftJoin('sekolah', 'sekolah.id', 'users.id_sekolah')
            ->select(
                [
                    'users.*',
                    'sekolah.*',
                    'users.status as ust'
                    ]
            )
            ->get();
        
        return view('page.sekolah.admin_sekolah')->with('data', $data);
    }

    public function AddAdminSekolah(Request $r)
    {
        if($r->user_password != $r->user_repassword) {
            return redirect('/page/sekolah/admin')->with('error', 'Password tidak sama, silahkan input ulang password !');
        } else {
            try{
                $data               = new User;
                $data->name         = $r->user_nama;
                $data->id_sekolah   = $r->user_sekolah;
                $data->type         = "admin_sekolah";
                $data->email        = $r->user_email;
                $data->password     = bcrypt($r->user_password);
                $data->save();
                return redirect('/page/sekolah/admin');
            } catch(Exeption $e) {
                return redirect('/page/sekolah/admin')->with('error', 'Error : '.$e);
            }
        }
    }

    public function EditAdminSekolah(Request $r)
    {
        $data = User::find($r->id);

    
        return view('page.sekolah.admin_sekolah_edit')->with('data', $data);
    }

    public function EditAdminSekolahSave(Request $r)
    {


        $data               = User::find($r->idadminsekolah);
        $data->name         = $r->user_nama;
        $data->nip          = $r->user_nip;
        $data->alamat       = $r->user_alamat;
        $data->foto         = $r->user_foto;
        $data->email        = $r->user_email;
        $data->password     = bcrypt($r->user_password);
        $data->save();

        return redirect('/page/sekolah/admin');


    }

    public function DeleteAdminSekolah($id)
    {

        $data = User::findOrFail($id);


        $data -> delete();

        return redirect('/page/sekolah/admin');
    }

    //Tahun Ajaran

    public function ShowTahunAjaran(Request $r)
    {
        $data = TahunAjaran::all();
        
        return view('page.sekolah.tahun_ajaran')->with('data', $data);
    }

    public function AddTahunAjaran(Request $r)
    {

        $data                   = new TahunAjaran;
        $data->id_admin         = 1;        
        $data->ta_semester      = $r->tahun_semester;
        $data->ta_tahun_ajaran  = $r->tahun_tahunajaran;
        $data->save();

        return redirect('/page/sekolah/tahun-ajaran');


    }

    public function EditTahunAjaran(Request $r)
    {
        $data = TahunAjaran::find($r->id);

    
        return view('page.sekolah.admin_sekolah_edit')->with('data', $data);
    }

    public function EditTahunAjaranSave(Request $r)
    {


        $data               = TahunAjaran::find($r->idadminsekolah);
        $data->name         = $r->user_nama;
        $data->nip          = $r->user_nip;
        $data->alamat       = $r->user_alamat;
        $data->foto         = $r->user_foto;
        $data->email        = $r->user_email;
        $data->password     = bcrypt($r->user_password);
        $data->save();

        return redirect('/page/sekolah/tahun-ajaran');


    }

    public function DeleteTahunAjaran($id)
    {

        $data = TahunAjaran::findOrFail($id);


        $data -> delete();

        return redirect('/page/sekolah/tahun-ajaran');
    }
}
