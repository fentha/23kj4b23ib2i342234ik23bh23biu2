<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gls;
use App\Perpustakaan;
use App\Siswa;
use App\Http\Controllers\PandoraController as Pandora;

class GlsController extends Controller
{
    public function ShowGlsMelihat()
    {
        $data = Gls::where('gls_kategori', 'melihat')
            ->leftJoin('siswa', 'siswa.id', 'gls.id_siswa')
            ->select(
                [
                    'gls.*',
                    'siswa.*',
                    'gls.created_at as cat'
                    ]
            )
            ->get();
        
        return view('page.gls.melihat')->with('data', $data);
    }

    public function DeleteMelihat($id)
    {

        $data = Gls::findOrFail($id);


        $data -> delete();

        return redirect('/page/gls/melihat');
    }

    public function ShowGlsMembaca()
    {
        $data = Gls::where('gls_kategori', 'membaca')
            ->leftJoin('siswa', 'siswa.id', 'gls.id_siswa')
            ->leftJoin('perpustakaan', 'perpustakaan.id', 'gls.id_buku')
            ->select(
                [
                    'gls.*',
                    'siswa.*',
                    'perpustakaan.id',
                    'perpustakaan.perpustakaan_judul_buku',
                    'gls.created_at as cat'
                    ]
            )
            ->get();
        
        return view('page.gls.membaca')->with('data', $data);
    }

    public function DeleteMembaca($id)
    {

        $data = Gls::findOrFail($id);


        $data -> delete();

        return redirect('/page/gls/membaca');
    }

    public function ShowGlsMendengar()
    {
        $data = Gls::where('gls_kategori', 'mendengar')
            ->leftJoin('siswa', 'siswa.id', 'gls.id_siswa')
            ->select(
                [
                    'gls.*',
                    'siswa.*',
                    'gls.created_at as cat'
                    ]
            )
            ->get();
        
        return view('page.gls.mendengar')->with('data', $data);
    }

    public function DeleteMendengar($id)
    {

        $data = Gls::findOrFail($id);


        $data -> delete();

        return redirect('/page/gls/mendengar');
    }

    /*======================================
    =            API Controller            =
    ======================================*/
    
    public function saveGlsMembaca(Request $r)
    {
        $idbuku                 = Perpustakaan::where('perpustakaan_barcode', $r->barcodeBuku)->get()->first()->id;
        $idsiswa                = Siswa::where('siswa_nis', $r->nis)->get()->first();
        
        if($idsiswa != null && $idbuku != null) {
            $_data                   = new Gls();
            $_data->gls_kategori     = 'membaca';
            $_data->id_buku          = $idbuku;
            $_data->id_siswa         = $idsiswa->id;
            $_data->gls_halaman      = $r->gls_hal;
            $_data->gls_rangkuman    = $r->gls_rangkuman;
            $_data->save();

            return Pandora::RespondsData(200, null, null, "TERIMA KASIH ".strtoupper($idsiswa->siswa_nama).", DATA KAMU TELAH DISIMPAN.");
        } else {
            return Pandora::RespondsData(500, null, null, 'Data siswa dan buku tidak ditemukan');
        }
       
    }

    public function saveGlsMelihat(Request $r)
    {
        $idsiswa                = Siswa::where('siswa_nis', $r->nis)->get()->first();

        if($idsiswa != null) {
            $_data                   = new Gls();
            $_data->gls_kategori     = 'melihat';
            $_data->id_siswa         = $idsiswa->id;
            $_data->gls_rangkuman    = $r->gls_rangkuman;
            $_data->save();

            return Pandora::RespondsData(200, null, null, "TERIMA KASIH ".strtoupper($idsiswa->siswa_nama).", DATA KAMU TELAH DISIMPAN.");
        } else {
            return Pandora::RespondsData(500, null, null, 'Data siswa tidak ditemukan');
        }
    }

    public function saveGlsMendengar(Request $r)
    {
        $idsiswa                = Siswa::where('siswa_nis', $r->nis)->get()->first();

        if($idsiswa != null) {
            $_data                   = new Gls();
            $_data->gls_kategori     = 'mendengar';
            $_data->id_siswa         = $idsiswa->id;
            $_data->gls_rangkuman    = $r->gls_rangkuman;
            $_data->save();

            return Pandora::RespondsData(200, null, null, "TERIMA KASIH ".strtoupper($idsiswa->siswa_nama).", DATA KAMU TELAH DISIMPAN.");
        } else {
            return Pandora::RespondsData(500, null, null, 'Data siswa tidak ditemukan');
        }
    }
    

}
