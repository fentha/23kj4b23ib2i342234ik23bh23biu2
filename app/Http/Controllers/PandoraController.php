<?php

namespace App\Http\Controllers;
use Redirect;
use Illuminate\Http\Request;

class PandoraController extends Controller
{
    public static function RedirectTo($status = null, $msg = null, $url = null, $data = null)
    {
        if($url == null) {
            return Redirect::to(url()->previous())
                    ->with('status', $status)
                    ->with('data', $data)
                    ->with('msg', $msg);
        } else {
            return Redirect::to($url)
                ->with('status', $status)
                ->with('data', $data)
                ->with('msg', $msg);
        }
        
    }

    public static function RespondsData($status = null, $data = null, $token = null, $msg = null)
    {
        $success['status']      = $status;
        $success['data']         = $data;
        $success['token']         = $token;
        $success['message']     = $msg;
        return response()->json(['status'=>$success['status'], 'token'=>$success['token'], 'data'=>$success['data'], 'message'=>$success['message']]);
    }

    public static function setDateToDB($data)
    {
        if(isset($data) && $data != null) {
            $data_ = explode("-", $data);
            $data_ = $data_[2]."-".$data_[1]."-".$data_[0];
            return $data_;
        } else {
            return $data;
        }
    }

    public static function nullDataReturn($data)
    {
        if($data == null) {
            return "TIdak terdapat data untuk saat ini.";
        } else {
            return $data;
        }
    }

    public static function transformToRegionDate($data)
    {
        if(isset($data) && $data != null) {
            $data_ = date('d-m-Y', strtotime($data));
            return $data_;
        } else {
            return $data;
        }
    }

    public static function bulanIndonesia($month)
    {
        switch ($month) {
        case '01':
            echo "Januari";
            break;

        case '02':
            echo "Februari";
            break;

        case '03':
            echo "Maret";
            break;

        case '04':
            echo "April";
            break;
                
        case '05':
            echo "Mei";
            break;

        case '06':
            echo "Juni";
            break;

        case '07':
            echo "Juli";
            break;

        case '08':
            echo "Agustus";
            break;

        case '09':
            echo "September";
            break;

        case '10':
            echo "Oktober";
            break;

        case '11':
            echo "November";
            break;   

        case '12':
            echo "Desember";
            break; 
            
        default:
            echo $month;
            break;
        }
    }

}
