<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;

class AjaxController extends Controller
{
    function generatorId(Request $r)
    {
        $data["id"] = Self::IDgenerator($r->_type);
        return json_encode($data);
    }

    private function IDgenerator($type)
    {
        $result = Self::getRandom($type);

        while(!Siswa::where("siswa_nis", $result)->get()){
            $result = Self::getRandom($type);
        }

        return $result;
    }

    private function getRandom($type)
    {
        $number = '';
        $result = '';

        for($i = 0; $i <= 8; $i++) {
            $number .= mt_rand(0, 9);
        }

        if($type == "siswa") {
            $result = "01".$number;
        } else {
            $result = "02".$number;
        }

        return $result;
    }
}
