<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public function ShowUsers()
    {
        $data = User::all();
        
        return view('page.user.main')->with('data', $data);
    }

    public function AddUsers(Request $r)
    {
        try {

            if($r->file('s_foto') != null) {
                    // Image settings
                $fimgPath = "images/guru_photo/";
                $fimgname = "photo_".$r->nip."_".time();
                $fimg     = $r->file('s_foto');
                $fimgext  = $fimg->getClientOriginalExtension();
                $fullPath = $fimgPath.$fimgname.".".$fimgext;

                    // Image compress and save
                    // \Image::make($fimg->save($fullPath)->resizeImage(1024, null));
                \Image::make($fimg)->resize(
                    1024, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(0)->save($fullPath);
                \Image::make($fimg)->resize(
                    250, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(0)->save("thumb_image/".$fullPath);

            } else {
                $fullPath = null;
            }

            $data               = new User;
            $data->id_sekolah   = $r->sekolah;
            $data->name         = $r->name;
            $data->nip          = $r->nip;
            $data->type         = $r->type;
            $data->foto         = $fullPath;
            $data->email        = $r->email;
            $data->password     = bcrypt($r->password);
            $data->save();

            return redirect('/page/user');

        } catch (Exception $e){

            
            return redirect('/page/user');
        }
    }

    public function DeleteUsers($id)
    {

        $data = User::findOrFail($id);


        $data -> delete();

        return redirect('/page/user');
    }

    public function EditUsers(Request $r)
    {
        $data = User::find($r->id);

    
        return view('page.user.edit')->with('data', $data);
    }

    public function EditUsersSave(Request $r)
    {
        try {

            if($r->file('s_foto') != null) {
                // Image settings
                $fimgPath = "images/guru_photo/";
                $fimgname = "photo_".$r->nip."_".time();
                $fimg     = $r->file('s_foto');
                $fimgext  = $fimg->getClientOriginalExtension();
                $fullPath = $fimgPath.$fimgname.".".$fimgext;

                // Image compress and save
                // \Image::make($fimg->save($fullPath)->resizeImage(1024, null));
                \Image::make($fimg)->resize(
                    1024, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(0)->save($fullPath);
                \Image::make($fimg)->resize(
                    250, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(0)->save("thumb_image/".$fullPath);


                $data               = User::find($r->iduser);
                $data->name         = $r->name;
                $data->id_sekolah   = $r->sekolah;
                $data->nip          = $r->nip;
                $data->type         = $r->type;
                $data->foto         = $fullPath;            
                $data->email        = $r->email;
                if($r->password !== null || $r->password !== ""){
                    $data->password     = bcrypt($r->password);                    
                }
                $data->save();

                return redirect('/page/user');

            } else {

                $data               = User::find($r->iduser);
                $data->id_sekolah   = $r->sekolah;
                $data->name         = $r->name;
                $data->nip          = $r->nip;
                $data->type         = $r->type;           
                $data->email        = $r->email;
                if($r->password !== null || $r->password !== ""){
                    $data->password     = bcrypt($r->password);                    
                }
                $data->save();

                return redirect('/page/user');
            }

        } catch (Exception $e){
            return redirect('/page/user');
        
        }
    }
}
