<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OpsiPenilaianKarakter;
use App\Penilaian_Karakter;
use App\TemaPenilaianKarakter;
use App\Opsi_Pelanggaran;
use App\Pelanggaran_Siswa;
use App\Siswa;
use Auth;
use App\Http\Controllers\PandoraController as Pandora;

class KarakterController extends Controller
{

    public function ShowTemaKarakter()
    {
        $data = TemaPenilaianKarakter::all();
        return view('page.karakter.tema_penilaian_karakter')->with('data', $data);
    }

    public function AddTemaKarakter(Request $r)
    {
        $data = new TemaPenilaianKarakter();
        $data->id_sekolah       = Auth::user()->id_sekolah;
        $data->tema_penilaian   = $r->value_tema;
        $data->save();

        return redirect('/page/karakter/tema-penilaian-karakter')->with('msg', 'Tema penilaian karakter sukses disimpan.');
    }

    public function DeleteTemaKarakter(Request $r)
    {
        return redirect('/page/karakter/tema-penilaian-karakter')->with('msg', 'Tema penilaian karakter sukses disimpan.');
    }


    public function ShowOpsiPenilaianKarakter()
    {
        $data = OpsiPenilaianKarakter::select(
            [
                    'opsi_penilaian_karakter.*',
                    'tema_penilaian_karakter.*',
                    'opsi_penilaian_karakter.id as opid',
            ]
        )
            ->leftJoin('tema_penilaian_karakter', 'tema_penilaian_karakter.id', 'opsi_penilaian_karakter.id_tema_penilaian_karakter')
            ->orderBy('tema_penilaian', 'asc')
            ->get();
        return view('page.karakter.opsi_penilaian_karakter')->with('data', $data);
    }

    public function AddOpsiKarakter(Request $r)
    {

        $data                               = new OpsiPenilaianKarakter;
        $data->id_sekolah                   = Auth::user()->id_sekolah;
        $data->id_admin                     = Auth::user()->id;
        $data->id_tema_penilaian_karakter   = $r->opsi_tema;
        $data->opsi_konten                  = $r->opsi_konten;
        $data->opsi_bobot_nilai             = $r->opsi_nilai;
        $data->save();

        return redirect('/page/karakter/opsi-penilaian-karakter');

    }

    public function DeleteOpsiKarakter($id)
    {
        $data = OpsiPenilaianKarakter::findOrFail($id);
        $data->delete();
        return redirect('/page/karakter/opsi-penilaian-karakter');
    }

    public function ShowPenilaianKarakter()
    {
        $data = Penilaian_karakter::all();
        
        return view('page.karakter.penilaian_karakter')->with('data', $data);
    }

    //pelanggaran
    
    public function AddPelanggaran(Request $r)
    {

        $data                       = new Opsi_Pelanggaran;
        $data->id_admin             = Auth::user()->id;
        $data->nama_pelanggaran     = $r->pelanggaran_nama;
        $data->bobot_pelanggaran    = $r->pelanggaran_point;
        $data->save();

        return redirect('/page/karakter/opsi-pelanggaran');

    }

    public function ShowOpsiPelanggaran()
    {
        $data = Opsi_Pelanggaran::all();
        
        return view('page.karakter.opsi_pelanggaran')->with('data', $data);
    }

    public function DeletePelanggaran($id)
    {

        $data = Opsi_Pelanggaran::findOrFail($id);


        $data -> delete();

        return redirect('/page/karakter/opsi_pelanggaran');
    }

    public function ShowPelanggaranSiswa()
    {
        $data = Pelanggaran_Siswa::select(
            [
                    'pelanggaran_siswa.*',
                    'siswa.*',
                    'opsi_pelanggaran.*',
                    'pelanggaran_siswa.id as opid',
            ]
        )
            ->leftJoin('opsi_pelanggaran', 'opsi_pelanggaran.id', 'pelanggaran_siswa.id_opsi_pelanggaran')
            ->leftJoin('siswa', 'siswa.id', 'pelanggaran_siswa.id_siswa')
            ->orderBy('nama_pelanggaran', 'asc')
            ->get();
        return view('page.karakter.pelanggaran')->with('data', $data);
    }

    public function AddPelanggaranSiswa(Request $r)
    {
        $siswa      = explode(" - ", $r->pilih_siswa);
        $siswaID    = Siswa::where('siswa_nis', $siswa[0])->get()->first()->id; 

        $data                       = new Pelanggaran_Siswa;
        $data->id_admin             = Auth::user()->id;
        $data->id_siswa             = $siswaID;       
        $data->id_ta                = \App\TahunAjaran::where('ta_status', 1)->get()->first()->id;       
        $data->id_opsi_pelanggaran  = $r->pilih_pelanggaran;
        $data->save();

        return redirect('/page/karakter/pelanggaran-siswa');

    }

    public function DeletePelanggaranSiswa($id)
    {
        $data = Pelanggaran_Siswa::findOrFail($id);
        $data->delete();
        return redirect('/page/karakter/pelanggaran-siswa');
    }

    public function getSiswaList(Request $r)
    {
        $data_ = Siswa::whereRaw('UPPER(siswa_nama) like "%'.strtoupper($r->term).'%"')
                ->orWhere('siswa_nis', 'like', '%'.$r->term.'%')
                ->where('id_sekolah', Auth::user()->id_sekolah)
                ->groupBy('siswa_nama')
                ->get();

        foreach ($data_ as $data_val)
        {
            if($data_val['siswa_nama'] != "") {
                $data[] = array('id'=>$data_val['id'],'label'=>$data_val['siswa_nis']." - ".$data_val['siswa_nama'],'value'=>$data_val['siswa_nis']." - ".$data_val['siswa_nama']);
            }
        }

        return json_encode($data);
    }


    /*=======================================
    =            API Controllers            =
    =======================================*/
    
    public function listTemaPenilaianKarakter()
    {
        $_data = TemaPenilaianKarakter::select(['id','tema_penilaian'])->get();
        return Pandora::RespondsData(200, $_data, null, null);
    }

    public function listDetailTemaPenilaianKarakter(Request $r)
    {
        $_data = OpsiPenilaianKarakter::where('id_tema_penilaian_karakter', $r->idTema)
                ->select(
                    [
                    'id',
                    'opsi_konten',
                    ]
                )
                ->get();
        return Pandora::RespondsData(200, $_data, null, null);
    }

    public function savePenilaianKarakter(Request $r)
    {
        $d                              = new Penilaian_Karakter();
        $d->id_siswa                    = Siswa::where('siswa_nis', $r->idSiswa)->get()->first()->id;
        $d->id_opsi_penilaian_karakter  = $r->idOpsi;
        $d->save();

        $msg = "TERIMA KASIH TELAH MENILAI KARAKTER ANDA";
        return Pandora::RespondsData(200, null, null, $msg);
    }
}
