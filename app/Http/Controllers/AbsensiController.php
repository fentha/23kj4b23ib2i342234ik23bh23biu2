<?php

namespace App\Http\Controllers;
use App\Siswa;
use App\Absensi;
use App\Kelas;
use App\SettingAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\PandoraController as Pandora;
use Auth;
use DB;
use Validator;

class AbsensiController extends Controller
{

    public function ShowAbsensi(Request $request)
    {
        if(in_array('administrator', explode(",", Auth::user()->type))){
            $data = Kelas::leftJoin('users', 'users.id', 'kelas.id_user')
                    ->leftJoin('sekolah', 'sekolah.id', 'kelas.id_sekolah')
                    ->select(['users.name','sekolah.nama_sekolah','kelas.id as kid', 'kelas.*'])
                    ->get();
        } else {
            $data = Kelas::where('kelas.id_sekolah', Auth::user()->id_sekolah)
                    ->leftJoin('users', 'users.id', 'kelas.id_user')
                    ->leftJoin('sekolah', 'sekolah.id', 'kelas.id_sekolah')
                    ->select(['users.name','sekolah.nama_sekolah','kelas.id as kid', 'kelas.*'])
                    ->get();
        }
        
        return view('page.absensi.hari_ini')->with('data', $data);
    }

    public function ShowFindAbsensi($id)
    {
        $data = Kelas::find($id);
        return view('page.absensi.absensi',['data'=>$data]);
    }

    public function AbsensiSave(Request $request){
       
       
        $absen = new Absensi;
        $absen->id_siswa = Auth::siswa();
        $absen->id_kelas = Kelas::find(Auth::kelas()->id);
        $absen->absensi_waktu = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        $absen->absensi_status = $request->absen;
        $absen->absensi_keterangan = $request->absen;
        $absen->save();

        return redirect('/page/absensi/hari-ini');
    }

    //rekap



    public function ShowKelas($table)
    {
        if($table=="kelas"){
            $data = Kelas::get();
            return view('page.absensi.rekap_kelas', ['data' => $data]);
        }
    }

    public function DetailKelas($id)
    {
        $data = Kelas::find($id);
        return view('page.absensi.rekap_absensi',['data' => $data]);
    }




    function fetch_data(Request $request)
    {
        if($request->ajax()) {
            if($request->from_date != '' && $request->to_date != '') {
                $data = DB::table('absensi')
                    ->whereBetween('absensi_waktu', array($request->from_date, $request->to_date))
                    ->get();
            }
            else
            {
                $data = DB::table('absensi')->orderBy('date', 'desc')->get();
            }
            echo json_encode($data);
        }
    }

    /*======================================
    =            API Controller            =
    ======================================*/
    
    public function prosesAbsensi(Request $r)
    {
        $getData        = Siswa::where('siswa_nis', $r->nis)->get()->first();

        if($getData) {
            $toleransi      = \App\SettingAdmin::where('setting_title', 'set_toleransi_keterlambatan')->get()->first()->setting_value;
            $jammasuk       = \App\SettingAdmin::where('setting_title', 'set_jam_masuk')->get()->first()->setting_value;
            $time           = date("Y-m-d H:i:s");
            $set            = date("Y-m-d H:i:s", strtotime(date("Y-m-d ").$jammasuk." +".$toleransi." mins"));
            $sudahAbsensi   = false;
                
            // Cek keterlambatan
            if($time > $set) {
                $statusAbsensi  = "terlambat";
                if(SettingAdmin::where('setting_title', 'set_msg_absensi_terlambat')->get()->first()  !== null) {
                    $data       = strtoupper($getData->siswa_nama)."\n\n".SettingAdmin::where('setting_title', 'set_msg_absensi_terlambat')->get()->first()->setting_value;
                } else {
                    $data       = null;
                }
            } else {
                $statusAbsensi   = "masuk";
                if(SettingAdmin::where('setting_title', 'set_msg_absensi_tepat')->get()->first() !== null) {
                    $data        = strtoupper($getData->siswa_nama)."\n\n".SettingAdmin::where('setting_title', 'set_msg_absensi_tepat')->get()->first()->setting_value;
                } else {
                    $data        = null;
                }
            }

            // Cek absensi kepulangan
            $cekPulang = Absensi::where('id_siswa', $getData->id)->where('absensi_waktu', 'like', date('Y-m-d').'%')->where('absensi_status', 'terlambat')->orWhere('absensi_status', 'masuk')->get()->first();
            if($cekPulang) {
                if(date('Y-m-d H:i:s') < date('Y-m-d H:i:s', strtotime($cekPulang->absensi_waktu." +5 mins"))){
                    $sudahAbsensi = true;
                    $data = "Waktu kepulangan dengan kedatangan terlalu dekat. Silahkan tunggu 5 menit dari kedatangan atau pukul ".date('H:i:s', strtotime($cekPulang->absensi_waktu."+5 mins"))." WIB";
                } else {
                    $statusAbsensi  = "pulang";
                    if(SettingAdmin::where('setting_title', 'set_msg_absensi_kepulangan')->get()->first() !== null) {
                        $data            = strtoupper($getData->siswa_nama)."\n\n".SettingAdmin::where('setting_title', 'set_msg_absensi_kepulangan')->get()->first()->setting_value;
                    } else {
                        $data            = null;
                    }
                }
            }

            // Cek apabila telah melaksanakan absensi
            if(Absensi::where('id_siswa', $getData->id)->where('absensi_waktu', 'like', date('Y-m-d').'%')->where('absensi_status', 'pulang')->get()->first()) {
                $sudahAbsensi = true;
                $statusAbsensi  = "sudah_absensi";
                if(SettingAdmin::where('setting_title', 'set_msg_absensi_sudah')->get()->first() !== null) {
                    $data            = strtoupper($getData->siswa_nama)."\n\n".SettingAdmin::where('setting_title', 'set_msg_absensi_sudah')->get()->first()->setting_value;
                } else {
                    $data            = null;
                }
            }

            // Simpan data apabila absensi kedatangan
            if($sudahAbsensi == false) {
                $_data                      = new Absensi();
                $_data->id_siswa            = $getData->id;
                $_data->absensi_waktu       = date('Y-m-d H:i:s');
                $_data->absensi_status      = $statusAbsensi;
                $_data->save();
            } 
            return Pandora::RespondsData(200, $data, null, $statusAbsensi);
        } else {
            return Pandora::RespondsData(500, null, null, 'Data siswa tidak ditemukan');
        }
    }

    public function hitung(Request $request){
        $resource = Kelas::find($request->id);
        foreach($resource->siswa as $index => $res){
            $alfa=Absensi::where(['siswa_id'=>$res->id_siswa])->where('status','Alfa');
            $izin=Absensi::where(['siswa_id'=>$res->id_siswa])->where('status','Izin');
            $sakit=Absensi::where(['siswa_id'=>$res->id_siswa])->where('status','Sakit');
            if($request->tanggal_awal != 0){
                $alfa=$alfa->where('tanggal','>=',$request->tanggal_awal);
                $izin=$izin->where('tanggal','>=',$request->tanggal_awal);
                $sakit=$sakit->where('tanggal','>=',$request->tanggal_awal);
            }
            if($request->tanggal_akhir != 0){
                $alfa=$alfa->where('tanggal','<=',$request->tanggal_akhir);
                $izin=$izin->where('tanggal','<=',$request->tanggal_akhir);
                $sakit=$sakit->where('tanggal','<=',$request->tanggal_akhir);
            }
            $alfa=$alfa->get();
            $izin=$izin->get();
            $sakit=$sakit->get();
            
        ?>
        <tr>
            <td class="text-center"><?php echo $index+1 ?></td>
            <td><?php echo $res->nis ?></td>
            <td><?php echo $res->nama ?></td>
            <td><?php echo count($alfa) ?></td>
            <td><?php echo count($izin) ?></td>
            <td><?php echo count($sakit) ?></td>
        </tr>
        <?php

        }
    }


    
}
