<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KartuSetting;
use App\DesignKartu;
use App\Siswa;
use App\Sekolah;
use App\User;
class KartuController extends Controller
{
    public function designPage()
    {
        $data = DesignKartu::all();
        return view('page.kartu.design')->with('data', $data);
    }

    public function designSave(Request $r)
    {

        try {

            if($r->file('design_kartu') != null) {
                // Image settings
                $fimgPath = "images/design_kartu/";
                $fimgname = "design_".$r->id."_".time();
                $fimg     = $r->file('design_kartu');
                $fimgext  = $fimg->getClientOriginalExtension();
                $fullPath = $fimgPath.$fimgname.".".$fimgext;

                // Image compress and save
                // \Image::make($fimg->save($fullPath)->resizeImage(1024, null));
                \Image::make($fimg)->resize(
                    1024, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(0)->save($fullPath);
                \Image::make($fimg)->resize(
                    250, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(0)->save("thumb_design/".$fullPath);

            } else {
                $fullPath = null;
            }
       
            $data               = new DesignKartu;
            $data->design   = $fullPath;
            $data->save();

            return redirect('/page/kartu/design');

        } catch (Exception $e){

            
            return redirect('/page/kartu/design');
        }        
    }



    public function layoutSave(Request $r)
    {
        return redirect('/page/kartu/layout');
    }

    public function layoutSettingPage(Request $r)
    {
        $data         = KartuSetting::where('id_design', $r->designid)->where('id', $r->layoutid)->get();
        $setting     = json_decode($data->first()->setting_value);
        $element    = $setting->data;

        return view('page.kartu.layout_setting')
            ->with('setting', $setting)
            ->with('element', $element)
            ->with('data', $data);
    }


    public function cetakPage()
    {
        $data = Sekolah::all();
        return view('page.kartu.cetak')->with('data', $data);
    }

    public function cetakKartu(Request $r)
    {
        // $r->type
        // $r->idsekolah
        if($r->type == 'siswa'){
            $data =  Siswa::where('id_sekolah', base64_decode($r->idsekolah))->get();            
            return view('page.siswa.cetak_qrcode_all')->with('data', $data)->with('type','siswa');
        } else if($r->type == 'guru'){
            $data =  User::where('id_sekolah', base64_decode($r->idsekolah))
                        ->where('type','guru')
                        ->orWhere('type','wali_kelas')->get();            
            return view('page.siswa.cetak_qrcode_all')->with('data', $data)->with('type','guru');
        } else if($r->type == 'admin' || $r->type == 'perpustakaan'){
            $data =  User::where('id_sekolah', base64_decode($r->idsekolah))
                        ->where('type','admin_sekolah')
                        ->orWhere('type','perpustakaan')->get();            
            return view('page.siswa.cetak_qrcode_all')->with('data', $data)->with('type','admin_sekolah');
        }

    }

    public function designLayoutFrontDashboard(Request $r)
    {
        $setting    = null;
        $element    = null;
        $data       = KartuSetting::where('id_design', base64_decode($r->kid))
                        ->where('id', base64_decode($r->lid))
                        ->get();

        if(!$data->isEmpty()) {
            $setting    = json_decode($data->first()->setting_value);
            $element    = $setting->data;
        } 

        return view('page.kartu.layout_setting')
            ->with('setting', $setting)
            ->with('element', $element)
            ->with('data', $data);
    }
    
    public function designLayoutFrontDashboardSave(Request $r)
    {
        $data = [];
        for ($i=0; $i < count($r->set); $i++) { 
            $data[$i]['id'] = $i;
            $data[$i]['type'] = $r->type[$i];
            $data[$i]['css'] = $r->set[$i];
        }
        var_dump($data);
    }
    
    public function designBgFrontDashboardSave(Request $r)
    {
        try {

            if($r->file('f_bg') != null) {
                // Image settings
                $fimgPath = "images/design_kartu/";
                $fimgname = "design_front_bg_".time();
                $fimg     = $r->file('f_bg');
                $fimgext  = $fimg->getClientOriginalExtension();
                $fullPath = $fimgPath.$fimgname.".".$fimgext;

                // Image compress and save
                // \Image::make($fimg->save($fullPath)->resizeImage(1024, null));
                \Image::make($fimg)->resize(
                    2048, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(0)->save($fullPath);
                \Image::make($fimg)->resize(
                    250, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(0)->save("thumb_design/".$fullPath);

            } else {
                $fullPath = null;
            }
       
            $setting['img']  = $fullPath;
            $setting['data'] = null; 

            $data                = new KartuSetting;
            $data->id_design     = $r->k_id;
            $data->setting_title = 'd_front';
            $data->setting_value = json_encode($setting);
            $data->save();

            return redirect('/page/kartu/design');

        } catch (Exception $e){
            abort(403, 'Kesalahan Upload');
        }  
    }
    
    public function designLayoutBackDashboard(Request $r)
    {

    }
    
    public function designLayoutBackDashboardSave(Request $r)
    {

    }
    
    public function designBgBackDashboardSave(Request $r)
    {
        try {

            if($r->file('b_bg') != null) {
                // Image settings
                $fimgPath = "images/design_kartu/";
                $fimgname = "design_back_bg_".time();
                $fimg     = $r->file('b_bg');
                $fimgext  = $fimg->getClientOriginalExtension();
                $fullPath = $fimgPath.$fimgname.".".$fimgext;

                // Image compress and save
                // \Image::make($fimg->save($fullPath)->resizeImage(1024, null));
                \Image::make($fimg)->resize(
                    2048, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(0)->save($fullPath);
                \Image::make($fimg)->resize(
                    250, null, function ($constraint) {
                        $constraint->aspectRatio(); 
                    }
                )->rotate(0)->save("thumb_design/".$fullPath);

            } else {
                $fullPath = null;
            }
       
            $setting['img']  = $fullPath;
            $setting['data'] = null; 

            $data                = new KartuSetting;
            $data->id_design     = $r->k_id;
            $data->setting_title = 'd_back';
            $data->setting_value = json_encode($setting);
            $data->save();

            return redirect('/page/kartu/design');

        } catch (Exception $e){
            abort(403, 'Kesalahan Upload');
        } 
    }
    



}
