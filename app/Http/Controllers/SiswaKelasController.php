<?php

namespace App\Http\Controllers;

use App\Sekolah;
use App\Kelas;
use App\Siswa;
use App\User;
use App\SiswaKelas;
use App\TahunAjaran;
use auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiswaKelasController extends Controller
{
    public function add($id){
        $resource=Siswa::get();
        return view('page/sekolah/siswa_kelas',['resource'=>$resource, 'kelas'=>$id]);
    }

    public function create(Request $request)
    {
        $check = SiswaKelas::where(['id_kelas' => $request->kelas, 'id_siswa' => $request->siswa])->get();
        if($check->count()>0){
            session()->flash('notif', array('success' => false, 'msgaction' => 'Tambah Data Gagal, Data Telah Ada!'));
            return redirect('/page/sekolah/kelas/');
        }
        else{
            $SK = new SiswaKelas;
            $SK->id_siswa = $request->siswa;
            $SK->id_kelas = $request->kelas;
            $SK->id_tahun_ajaran = 1 ;
            if($SK->save()){
                session()->flash('notif', array('success' => true, 'msgaction' => 'Tambah Data Berhasil!'));
            }
            else{
                session()->flash('notif', array('success' => false, 'msgaction' => 'Tambah Data Gagal, Silahkan Ulangi!'));
            }
            return redirect('/page/sekolah/kelas/'.$request->kelas);
        }
    }
}
