<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SettingAdmin;
use View;
use App\Http\Controllers\PandoraController as Pandora;
use Auth;

class SettingController extends Controller
{
    public function ShowSetting()
    {
        $data = SettingAdmin::all();
        return view::make('page.setting.main')->with('data', $data);
    }

    public function EditSetting(Request $r)
    {
        try {
            foreach ($r->input() as $key => $value) {
                if($key !== '_token') {
                    if(SettingAdmin::where('setting_title', $key)->exists() === false) {
                        $data = new SettingAdmin();
                        // $data->id_admin      = Auth::user()->id;
                        $data->id_admin      = Auth::user()->id;
                        $data->setting_title = $key;
                        $data->setting_value = $value;
                        $data->save();
                    } else {
                        $data = SettingAdmin::where('setting_title', $key)->get()->first();
                        // $data->id_admin      = Auth::user()->id;
                        $data->id_admin      = Auth::user()->id;
                        $data->setting_title = $key;
                        $data->setting_value = $value;
                        $data->save();
                    }
                }
            }
            return Pandora::RedirectTo(200, "Data pengaturan telah disimpan", null); 
        } catch (Exception $e) {
            return Pandora::RedirectTo("error", "Terjadi kesalahan, silahkan cek kembali data yang anda inputkan atau hubungi operator. Detail : ".$e->getMessage());
        }
    }
}
