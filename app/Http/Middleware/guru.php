<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;

class guru
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $type = explode(",", Auth::user()->type);
        if(in_array('guru', $type)){
            return $next($request);            
        } else {
            Auth::logout();
            $msg['title'] = 'Terjadi Kesalahan';
            $msg['text'] = 'Anda tidak memiliki otoritas pada halaman ini, silahkan login kembali';
            $msg['footer'] = null;
            $msg['status'] = 500;
            $data = "ola";

            return redirect('/login')->with('data', $msg);
        }
    }
}
