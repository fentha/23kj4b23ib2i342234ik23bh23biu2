<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class walikelas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        i$type = explode(",", Auth::user()->type);
        if(in_array('wali_kelas', $type)){
            return $next($request);            
        } else {
            Auth::logout();
            $msg['title'] = 'Terjadi Kesalahan';
            $msg['text'] = 'Anda tidak memiliki otoritas pada halaman ini, silahkan login kembali';
            $msg['footer'] = null;
            $msg['status'] = 500;
            $data = "ola";

            return redirect('/login')->with('data', $msg);
        }
    }
}
